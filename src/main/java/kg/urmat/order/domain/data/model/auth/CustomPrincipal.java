package kg.urmat.order.domain.data.model.auth;

import kg.urmat.order.domain.data.util.enums.RoleCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.Collection;
import java.util.Set;

@Getter
@Setter
public class CustomPrincipal implements UserDetails {

    @Serial
    private static final long serialVersionUID = -9138572089805652341L;

    private Long userId;
    private String username;
    private String password;
    private String name;
    private String surname;
    private boolean enabled;
    private Long cartId;
    private Set<GrantedAuthority> authorities;
    private Set<RoleCode> roleCodes;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
