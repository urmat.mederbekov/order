package kg.urmat.order.domain.data.util.enums;

import lombok.Getter;

@Getter
public enum AuthorityCode {
    CREATE,
    UPDATE,
    READ
}
