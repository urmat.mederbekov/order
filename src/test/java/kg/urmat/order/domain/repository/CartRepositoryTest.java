package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.NoSuchElementException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CartRepositoryTest {

    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private UserRepository userRepository;

    static Cart cart;
    static User user;

    @BeforeAll
    static void setUp() {
        user = new User();
        user.setCreatedAt(OffsetDateTime.now());
        user.setLogin("user1");
        user.setName("john");
        user.setSurname("doe");
        user.setPassword("password");
        user.setBirthDate(LocalDate.now().minusYears(18));

        cart = new Cart();
        cart.setCreatedAt(OffsetDateTime.now());
    }

    @Test
    void shouldFindByCreatedById() {
        user =  userRepository.save(user);
        cart.setUser(user);
        cartRepository.save(cart);
        assertThat(cart).isSameAs(cartRepository.findByUserId(user.getId()).get());
    }

    @Test
    void shouldNotFindByCreatedById() {
        assertThrows(NoSuchElementException.class, () -> cartRepository.findByUserId(user.getId()).get());
    }
}