package kg.urmat.order.domain.service;

import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.repository.RoleRepository;
import kg.urmat.order.domain.service.impl.RoleServiceImpl;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RoleServiceImplTest {

    @Mock
    private RoleRepository repository;
    @InjectMocks
    private RoleServiceImpl service;

    private final long id = 1L;

    @Test
    void shouldGet() {
        when(repository.findById(id)).thenReturn(Optional.of(new Role()));
        assertThat(service.get(id)).isNotNull();
    }

    @Test
    void shouldNotGetByCode() {
        when(repository.findById(id)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.get(id))
                .isInstanceOf(NoSuchElementFoundException.class)
                .hasMessage(String.format("No Role entity with id %d found", id));
    }

    @Test
    void shouldGetAllByCodes() {
        List<RoleCode> codes = List.of(RoleCode.USER, RoleCode.ADMIN);
        when(repository.findByCodeIn(codes)).thenReturn(Set.of(new Role()));
        assertEquals(1, service.getAllByCodes(codes).size());
    }

    @Test
    void shouldNotGetAllByCodes() {
        List<RoleCode> codes = List.of(RoleCode.USER, RoleCode.ADMIN);
        when(repository.findByCodeIn(codes)).thenReturn(Set.of());
        assertEquals(0, service.getAllByCodes(codes).size());
    }
}