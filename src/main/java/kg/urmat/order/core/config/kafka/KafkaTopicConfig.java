package kg.urmat.order.core.config.kafka;

import kg.urmat.order.core.config.property.KafkaProperties;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "kafka.enabled", havingValue = "true", matchIfMissing = true)
@RequiredArgsConstructor
public class KafkaTopicConfig {

    private final KafkaProperties properties;

    @Bean
    public NewTopic order(){
        return new NewTopic(
                properties.topic().name(),
                properties.topic().numPartitions(),
                properties.topic().replicationFactor()
        );
    }
}
