package kg.urmat.order.domain.service.auth.impl;

import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import kg.urmat.order.domain.service.auth.PrincipalContextProvider;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import static java.util.Objects.nonNull;

@Service
public class PrincipalContextProviderImpl implements PrincipalContextProvider {

    @Override
    public CustomPrincipal getCurrentPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return isAuthenticated() ? (CustomPrincipal) authentication.getPrincipal() : null;
    }

    @Override
    public boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return nonNull(authentication) && !AnonymousAuthenticationToken.class.
                isAssignableFrom(authentication.getClass()) && authentication.isAuthenticated();
    }

    @Override
    public boolean isAllowed(Long userId){
        CustomPrincipal currentPrincipal = getCurrentPrincipal();
        boolean isAdmin = currentPrincipal.getRoleCodes().contains(RoleCode.ADMIN);
        return isAdmin || userId.equals(currentPrincipal.getUserId());
    }
}
