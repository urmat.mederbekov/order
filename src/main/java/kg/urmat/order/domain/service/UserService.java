package kg.urmat.order.domain.service;

import kg.urmat.order.domain.data.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface UserService {

    Page<User> search(Specification<User> specification , Pageable pageable);

    User get(Long id);

    User getByLogin(String login);

    User save(User entity);

    boolean existsByLogin(String login);
}
