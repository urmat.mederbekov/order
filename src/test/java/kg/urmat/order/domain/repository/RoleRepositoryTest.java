package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class RoleRepositoryTest {

    @Autowired
    private RoleRepository repository;

    @Test
    void shouldFindByCode() {
        assertThat(repository.findByCode(RoleCode.ADMIN).get()).isNotNull();
    }

    @Test
    void shouldFindByCodeIn() {
        assertEquals(2, repository.findByCodeIn(List.of(RoleCode.ADMIN, RoleCode.USER)).size());
    }
}