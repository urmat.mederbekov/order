package kg.urmat.order.domain.facade;

import kg.urmat.order.domain.data.dto.auth.AuthRequest;
import kg.urmat.order.domain.data.dto.auth.CustomerPrincipalResponse;
import kg.urmat.order.domain.data.dto.user.UserRegisterRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;

public interface AuthFacade {

    CustomerPrincipalResponse login(AuthRequest loginRequest);

    UserResponse register(UserRegisterRequest userRequest);

}
