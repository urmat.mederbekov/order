package kg.urmat.order.domain.service.impl;

import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.repository.RoleRepository;
import kg.urmat.order.domain.service.RoleService;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Slf4j
@Service
@CacheConfig(cacheNames = "roles")
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Override
    @Cacheable(key = "#id")
    public Role get(Long id) {
        log.info("Getting Role with id {}", id);
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException("Role", "id", id));
    }

    @Override
    public Role getByCode(RoleCode code) {
        log.info("Getting Role with code {}", code);
        return repository.findByCode(code)
                .orElseThrow(() -> new NoSuchElementFoundException("Role", "code", code));
    }

    @Override
    public Set<Role> getAllByCodes(Collection<RoleCode> codes) {
        log.info("Getting Roles with codes {}", codes);
        return repository.findByCodeIn(codes);
    }
}
