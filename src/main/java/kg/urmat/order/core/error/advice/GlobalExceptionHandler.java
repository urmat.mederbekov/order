package kg.urmat.order.core.error.advice;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import kg.urmat.order.core.error.exception.InCartAlreadyExistsException;
import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.core.error.exception.UserAlreadyExistsException;
import kg.urmat.order.core.error.response.ApiError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value =  {
            MethodArgumentNotValidException.class,
            InvalidFormatException.class,
            IllegalArgumentException.class,
            ConstraintViolationException.class,
            UserAlreadyExistsException.class,
            InCartAlreadyExistsException.class
    })
    public ResponseEntity<ApiError> handleArgumentNotValidException(Exception ex) {
        List<String> errors;
        if (ex instanceof MethodArgumentNotValidException) {
            errors = ((MethodArgumentNotValidException) ex)
                    .getBindingResult()
                    .getAllErrors()
                    .stream()
                    .map(err -> err.unwrap(ConstraintViolation.class))
                    .map(err -> String.format("'%s' %s", err.getPropertyPath(), err.getMessage()))
                    .collect(Collectors.toList());
        } else {
            errors = Collections.singletonList(getField(ex.getMessage()));
        }
        var response = new ApiError(BAD_REQUEST, errors);
        log.error("Error response: {}", response);
        return new ResponseEntity<>(response, response.status());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ApiError> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex
    ) {
        var error = ex.getName() + " should be of type " + Objects.requireNonNull(ex.getRequiredType()).getName();
        var response = new ApiError(BAD_REQUEST, error);
        log.error("Error response: {}", response);
        return new ResponseEntity<>(response, response.status());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ApiError> handleAccessDeniedException(AccessDeniedException ex){
        var response = new ApiError(UNAUTHORIZED, ex.getMessage());
        log.error("Error response: {}", response);
        return new ResponseEntity<>(response, response.status());
    }

    @ExceptionHandler(NoSuchElementFoundException.class)
    public ResponseEntity<Object> handleNoSuchElementFoundException(NoSuchElementFoundException ex) {
        var response = new ApiError(NOT_FOUND, ex.getMessage());
        log.error("Error response: {}", response);
        return new ResponseEntity<>(response, response.status());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleException(Exception ex){
        var response = new ApiError(INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR.getReasonPhrase());
        log.error("Error message: {}",  ex.getMessage());
        return new ResponseEntity<>(response, response.status());
    }

    private String getField(String message){
        return message.substring(message.lastIndexOf(".") + 1);
    }
}
