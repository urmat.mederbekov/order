package kg.urmat.order.domain.service;

import jakarta.validation.ConstraintViolationException;
import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.repository.UserRepository;
import kg.urmat.order.domain.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository repository;
    @InjectMocks
    private UserServiceImpl service;

    private final String login = "admin";
    private final long id = 1L;

    @Disabled
    @Test
    void search() {
    }

    @Test
    void shouldGet() {
        when(repository.findById(id)).thenReturn(Optional.of(new User()));
        assertThat(service.get(id)).isNotNull();
    }

    @Test
    void shouldNotGet() {
        when(repository.findById(id)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.get(id))
                .isInstanceOf(NoSuchElementFoundException.class)
                .hasMessage(String.format("No User entity with id %d found", id));
    }

    @Test
    void shouldGetByLogin() {
        when(repository.findByLogin(login)).thenReturn(Optional.of(new User()));
        assertThat(service.getByLogin(login)).isNotNull();
    }

    @Test
    void itShouldNotGetByLogin() {
        when(repository.findByLogin(login)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.getByLogin(login))
                .isInstanceOf(NoSuchElementFoundException.class)
                .hasMessage(String.format("No User entity with login %s found", login));
    }

    @Test
    void shouldSave() {
        User user = new User();

        when(repository.save(user)).thenReturn(user);
        assertThat(service.save(user)).isNotNull();
    }

    @Test
    void shouldNotSave() {
        when(repository.save(any(User.class))).thenThrow(ConstraintViolationException.class);
        assertThrows(ConstraintViolationException.class, () -> service.save(new User()));
    }

    @Test
    void itShouldExistsByLogin() {
        when(repository.existsByLogin(login)).thenReturn(true);
        assertTrue(service.existsByLogin(login));
    }

    @Test
    void ItShouldNotExistsByLogin() {
        when(repository.existsByLogin(login)).thenReturn(false);
        assertFalse(service.existsByLogin(login));
    }
}