package kg.urmat.order.domain.service.impl;

import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.repository.ProductRepository;
import kg.urmat.order.domain.service.ProductService;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@CacheConfig(cacheNames = "products")
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;

    @Override
    public Page<Product> search(Specification<Product> build, Pageable pageable) {
        log.info("Searching products");
        return repository.findAll(build, pageable);
    }

    @Override
    @Cacheable(key = "#id")
    public Product get(Long id) {
        log.info("Getting product with id {}", id);
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException("Product", "id", id));
    }

    @Override
    public List<Product> getAllById(List<Long> ids) {
        log.info("Getting all products with ids {}", ids);
        return repository.findAllById(ids);
    }

    @Override
    public List<Product> getAllByCode(ProductCode code) {
        log.info("Getting all products with code {}", code);
        return repository.findAllByCode(code);
    }

    @Override
    @CachePut(key = "#entity.id", condition = "#entity.id == null")
    public Product save(Product entity) {
        log.info("Saving product {}", entity);
        return repository.save(entity);
    }
}
