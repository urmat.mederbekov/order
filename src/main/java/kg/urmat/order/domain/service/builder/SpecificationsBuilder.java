package kg.urmat.order.domain.service.builder;

import kg.urmat.order.domain.data.dto.page.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specification.where;

@RequiredArgsConstructor
public final class SpecificationsBuilder<T> {

    private final List<SearchCriteria> andParams;
    private final List<SearchCriteria> orParams;

    public Specification<T> build() {

        if(!andParams.isEmpty() && !orParams.isEmpty())
            return andSpecification().or(orSpecification());
        if(!andParams.isEmpty())
            return andSpecification();
        if(!orParams.isEmpty())
            return orSpecification();
        else
            return null;
    }

    public Sort andSort(List<Sort> criteria) {
        return criteria.stream().reduce(Sort::and).orElse(Sort.unsorted());
    }

    public List<Sort> generateSortList(List<SearchCriteria> criteria) {
        return criteria.stream().map(criterion -> switch (criterion.operation()) {
            case SORT_ASC -> Sort.by(Sort.Order.asc(criterion.field()));
            case SORT_DESC -> Sort.by(Sort.Order.desc(criterion.field()));
            default -> null;
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }
    private Specification<T> andSpecification() {
        return andParams.stream()
                .map(this::createSpec)
                .reduce(where(null), Specification::and);
    }

    private Specification<T> orSpecification() {
        return orParams.stream()
                .map(this::createSpec)
                .reduce(where(null), Specification::or);
    }


    private Specification<T> createSpec(SearchCriteria criteria){
        return (root, query, cb) -> switch (criteria.operation()) {
            case EQUALITY -> cb.equal(root.get(criteria.field()), criteria.value());
            case NEGATION -> cb.notEqual(root.get(criteria.field()), criteria.value());
            case GREATER_THAN -> cb.greaterThan(root.get(criteria.field()), criteria.value().toString());
            case LESS_THAN -> cb.lessThan(root.get(criteria.field()), criteria.value().toString());
            case LIKE -> cb.like(root.get(criteria.field()), criteria.value().toString());
            default -> null;
        };
    }

    public SpecificationsBuilder<T> withAnd(final SearchCriteria criteria) {
        andParams.add(criteria);
        return this;
    }

    public SpecificationsBuilder<T> withOr(final SearchCriteria criteria) {
        orParams.add(criteria);
        return this;
    }
}
