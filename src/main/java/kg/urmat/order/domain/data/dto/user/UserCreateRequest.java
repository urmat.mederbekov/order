package kg.urmat.order.domain.data.dto.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Past;
import kg.urmat.order.core.annotation.EnumContains;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import lombok.Builder;

import java.time.LocalDate;
import java.util.Set;

@Builder
public record UserCreateRequest(
        @NotBlank
        String login,
        @NotBlank
        String password,
        @NotBlank
        String name,
        @NotBlank
        String surname,
        @Past
        LocalDate birthDate,
        Set<@EnumContains(enumClass = RoleCode.class) RoleCode> roleCodes
) {}