package kg.urmat.order.domain.data.model;

import jakarta.persistence.*;
import kg.urmat.order.domain.data.model.base.AuditedEntity;
import kg.urmat.order.domain.data.util.enums.StatusCode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true, exclude = "orderItems")
@Entity
@Table(name = "orders")
@NamedEntityGraph(name = "order.orderItems", attributeNodes = @NamedAttributeNode(value = "orderItems"))
public class Order extends AuditedEntity {

    @Serial
    private static final long serialVersionUID = 807823371661881445L;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, unique = true)
    private StatusCode status = StatusCode.PROCESSING;

    private BigDecimal totalCost;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
