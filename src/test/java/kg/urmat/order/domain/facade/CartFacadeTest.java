package kg.urmat.order.domain.facade;

import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CartFacadeTest extends AbstractFacadeTest {

    @Autowired
    private CartFacade cartFacade;

    @Test
    void shouldGetByUserId() {
        long userId = 2L;
        var response = cartFacade.getByUserId(userId);
        assertEquals(userId, response.user().id());
    }

    @Test
    void shouldNotGetByUserId() {
        long userId = 1L;
        assertThatThrownBy(() -> cartFacade.getByUserId(userId))
                .isInstanceOf(NoSuchElementFoundException.class)
                .hasMessage(String.format("No Cart found, whose User id is %d", userId));
    }
}