package kg.urmat.order.domain.data.util.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RoleConstant {

    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
}
