package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.NoSuchElementException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class UserRepositoryTest {

    @Autowired
    private UserRepository repository;

    static User user;

    @BeforeAll
    static void setUp() {
        user = new User();
        user.setCreatedAt(OffsetDateTime.now());
        user.setLogin("user1");
        user.setName("john");
        user.setSurname("doe");
        user.setPassword("password");
        user.setBirthDate(LocalDate.now().minusYears(18));
    }

    @Test
    void shouldFindByLogin() {
        repository.save(user);
        assertThat(user).isSameAs(repository.findByLogin(user.getLogin()).get());
    }

    @Test
    void shouldNotFindByLogin() {
        assertThrows(NoSuchElementException.class, () -> repository.findByLogin(user.getLogin()).get());
    }

    @Test
    void shouldExistsByLogin() {
        repository.save(user);
        assertTrue(repository.existsByLogin("user1"));
    }

    @Test
    void shouldNotExistsByLogin() {
        assertFalse(repository.existsByLogin("user1"));
    }
}