package kg.urmat.order.domain.data.dto.cartIem;

import kg.urmat.order.domain.data.dto.product.ProductResponse;
import lombok.Builder;

@Builder
public record CartItemResponse(
        Long id,
        Long cartId,
        int quantity,
        ProductResponse product
){}