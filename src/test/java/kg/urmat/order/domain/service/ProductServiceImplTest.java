package kg.urmat.order.domain.service;

import jakarta.validation.ConstraintViolationException;
import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.repository.ProductRepository;
import kg.urmat.order.domain.service.impl.ProductServiceImpl;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @Mock
    private ProductRepository repository;
    @InjectMocks
    private ProductServiceImpl service;

    private final long id = 1L;

    @Disabled
    @Test
    void search() {
    }

    @Test
    void shouldGet() {
        when(repository.findById(id)).thenReturn(Optional.of(new Product()));
        assertThat(service.get(id)).isNotNull();
    }

    @Test
    void shouldNotGet() {
        when(repository.findById(id)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.get(id))
                .isInstanceOf(NoSuchElementFoundException.class)
                .hasMessage(String.format("No Product entity with id %d found", id));
    }

    @Test
    void shouldGetAllById(){
        when(repository.findAllById(List.of(1L))).thenReturn(List.of(new Product()));
        assertEquals(1, service.getAllById(List.of(1L)).size());
    }

    @Test
    void shouldAllGetByCode() {
        when(repository.findAllByCode(ProductCode.COMPUTER)).thenReturn(List.of(new Product()));
        assertEquals(1, service.getAllByCode(ProductCode.COMPUTER).size());
    }

    @Test
    void shouldNotGetAllById(){
        when(repository.findAllById(List.of(1L))).thenReturn(List.of());
        assertEquals(0, service.getAllById(List.of(1L)).size());
    }

    @Test
    void shouldNotGetAllByCode() {
        when(repository.findAllByCode(ProductCode.COMPUTER)).thenReturn(List.of());
        assertEquals(0, service.getAllByCode(ProductCode.COMPUTER).size());
    }

    @Test
    void shouldSave() {
        Product product = new Product();

        when(repository.save(product)).thenReturn(product);
        assertThat(service.save(product)).isNotNull();
    }

    @Test
    void shouldNotSave() {
        when(repository.save(any(Product.class))).thenThrow(ConstraintViolationException.class);
        assertThrows(ConstraintViolationException.class, () -> service.save(new Product()));
    }
}