package kg.urmat.order.domain.data.model;

import jakarta.persistence.*;
import kg.urmat.order.domain.data.model.base.BaseEntity;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "products")
public class Product extends BaseEntity {

    @Serial
    private static final long serialVersionUID = -4821415608813049705L;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, unique = true)
    private ProductCode code;

    @Column(nullable = false)
    private String name;

    private String description;

    private double price;

    private Long quantity;
}
