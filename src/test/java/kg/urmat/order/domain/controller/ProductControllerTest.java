package kg.urmat.order.domain.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kg.urmat.order.domain.data.dto.product.ProductCreateRequest;
import kg.urmat.order.domain.data.dto.product.ProductResponse;
import kg.urmat.order.domain.data.dto.product.ProductUpdateRequest;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import kg.urmat.order.domain.facade.ProductFacade;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static kg.urmat.order.domain.data.util.constant.AuthorityConstant.READ;
import static kg.urmat.order.domain.data.util.constant.RoleConstant.ADMIN;
import static kg.urmat.order.domain.data.util.constant.RoleConstant.USER;
import static kg.urmat.order.domain.data.util.constant.Route.PRODUCTS_API;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(ProductController.class)
class ProductControllerTest extends AbstractSecurityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ProductFacade productFacade;

    private final Long productId = 1L;

    @Test
    @Disabled
    void search() {
    }

    @Test
    @WithMockUser(authorities = READ)
    void shouldGet() throws Exception {
        var response = ProductResponse.builder()
                .name("mac")
                .code(ProductCode.COMPUTER)
                .price(1000.0)
                .description("expensive")
                .build();
        when(productFacade.get(1L)).thenReturn(response);

        mockMvc.perform(get(PRODUCTS_API + "/" + productId)
                        .contentType(APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.name").value(response.name()))
                .andExpect(jsonPath("$.code").value(response.code().name()))
                .andExpect(jsonPath("$.price").value(response.price()))
                .andExpect(jsonPath("$.description").value(response.description()));

        verify(productFacade, times(1)).get(1L);
    }

    @Test
    @WithMockUser(authorities = READ)
    void shouldNotGetNonExistentValue() throws Exception {
        when(productFacade.get(1L)).thenReturn(null);

        mockMvc.perform(get(PRODUCTS_API + "/" + productId)
                        .contentType(APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$").doesNotExist());

        verify(productFacade, times(1)).get(1L);
    }

    @Test
    void shouldNotGet() throws Exception {
        mockMvc.perform(get(PRODUCTS_API + "/" + productId)
                        .contentType(APPLICATION_JSON))
                .andExpect(status().is(403))
                .andExpect(jsonPath("$").doesNotExist());

        verify(productFacade, times(0)).get(productId);
    }

    @Test
    @WithMockUser(authorities = ADMIN)
    void shouldCreate() throws Exception {
        var request = ProductCreateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(100.0)
                .description("cheap")
                .name("hp")
                .build();
        var response = ProductResponse.builder()
                .code(ProductCode.COMPUTER)
                .price(100.0)
                .description("cheap")
                .name("hp")
                .build();

        when(productFacade.add(request)).thenReturn(response);

        mockMvc.perform(post(PRODUCTS_API)
                        .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.code").value(response.code().name()))
                .andExpect(jsonPath("$.price").value(response.price()))
                .andExpect(jsonPath("$.description").value(response.description()))
                .andExpect(jsonPath("$.name").value(response.name()));

        verify(productFacade, times(1)).add(request);
    }

    @Test
    @WithMockUser(authorities = USER)
    void shouldNotCreate() throws Exception {
        var request = ProductCreateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(100.0)
                .description("cheap")
                .name("hp")
                .build();

        mockMvc.perform(post(PRODUCTS_API)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(401));

        verify(productFacade, times(0)).add(request);
    }

    @Test
    @WithMockUser(authorities = ADMIN)
    void shouldUpdate() throws Exception {
        var request = ProductUpdateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(200.0)
                .description("cheap")
                .name("hp")
                .build();
        var response = ProductResponse.builder()
                .code(ProductCode.COMPUTER)
                .price(200.0)
                .description("cheap")
                .name("hp")
                .build();

        when(productFacade.update(request, productId)).thenReturn(response);

        mockMvc.perform(put(PRODUCTS_API + "/" + productId)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.code").value(response.code().name()))
                .andExpect(jsonPath("$.price").value(response.price()))
                .andExpect(jsonPath("$.description").value(response.description()))
                .andExpect(jsonPath("$.name").value(response.name()));

        verify(productFacade, times(1)).update(request, productId);
    }

    @Test
    @WithMockUser(authorities = USER)
    void shouldNotUpdate() throws Exception {
        var request = ProductUpdateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(200.0)
                .description("cheap")
                .name("hp")
                .build();

        mockMvc.perform(put(PRODUCTS_API + "/" + productId)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(401));

        verify(productFacade, times(0)).update(request, productId);
    }
}