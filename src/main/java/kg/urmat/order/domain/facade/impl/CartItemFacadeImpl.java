package kg.urmat.order.domain.facade.impl;

import kg.urmat.order.domain.data.dto.cartIem.CartItemCreateRequest;
import kg.urmat.order.domain.data.dto.cartIem.CartItemResponse;
import kg.urmat.order.domain.data.dto.cartIem.CartItemUpdateRequest;
import kg.urmat.order.core.error.exception.InCartAlreadyExistsException;
import kg.urmat.order.domain.facade.CartItemFacade;
import kg.urmat.order.domain.mapper.CartItemMapper;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.service.CartItemService;
import kg.urmat.order.domain.service.CartService;
import kg.urmat.order.domain.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class CartItemFacadeImpl implements CartItemFacade {

    private final CartItemService cartItemService;
    private final CartService cartService;
    private final ProductService productService;
    private final CartItemMapper cartItemMapper;

    @Override
    public CartItemResponse add(CartItemCreateRequest cartItemRequest, Long userId, Long cartId) {
        if (cartItemService.existsInCart(cartId, cartItemRequest.productId()))
            throw  new InCartAlreadyExistsException(cartItemRequest.productId(), cartId);
        Product product = productService.get(cartItemRequest.productId());
        Cart cart = cartService.getByUserId(userId);
        CartItem cartItem = cartItemService.save(cartItemMapper.toEntity(cartItemRequest, product, cart));
        return cartItemMapper.toResponsePublic(cartItem);
    }

    @Override
    public CartItemResponse update(CartItemUpdateRequest cartItemRequest, Long entityId, Long userId) {
        Cart cart = cartService.getByUserId(userId);
        CartItem cartItem = cartItemService.get(entityId);
        Product product = productService.get(cartItemRequest.productId());
        cartItem = cartItemService.save(cartItemMapper.toEntity(cartItemRequest, cartItem, product, cart));
        return cartItemMapper.toResponsePublic(cartItem);
    }
}
