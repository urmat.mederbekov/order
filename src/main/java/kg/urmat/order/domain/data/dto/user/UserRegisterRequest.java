package kg.urmat.order.domain.data.dto.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Past;
import lombok.Builder;

import java.time.LocalDate;

@Builder
public record UserRegisterRequest(
        @NotBlank
        String login,
        @NotBlank
        String password,
        @NotBlank
        String name,
        @NotBlank
        String surname,
        @Past LocalDate birthDate
){}