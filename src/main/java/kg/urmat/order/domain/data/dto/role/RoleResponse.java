package kg.urmat.order.domain.data.dto.role;

import kg.urmat.order.domain.data.util.enums.AuthorityCode;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import lombok.Builder;

import java.util.Set;

@Builder
public record RoleResponse(
        RoleCode code,
        String name,
        Set<AuthorityCode> authorities
){}