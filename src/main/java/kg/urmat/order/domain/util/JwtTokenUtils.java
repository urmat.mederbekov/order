package kg.urmat.order.domain.util;

import io.jsonwebtoken.*;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.Date;
import java.util.function.Function;

@Slf4j
public final class JwtTokenUtils {

    private static final String jwtSecret = "zdtlD3JK56m6wTTgsNFhqzjqP";
    private static final String jwtIssuer = "example.io";
    private static final Long jwtExpirationInSeconds = 7 * 24 * 60 * 60L;


    public static String generateToken(CustomPrincipal principal) {

        return Jwts.builder()
                .setSubject(principal.getUsername())
                .claim("authorities", principal.getAuthorities())
                .setIssuer(jwtIssuer)
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plusSeconds(jwtExpirationInSeconds)))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public static String getUsername(String token) {
        return getClaims(token, Claims::getSubject);

    }

    public Date getExpirationDate(String token) {
        return getClaims(token, Claims::getExpiration);
    }

    private static  <T> T getClaims(String token, Function<Claims, T> claimsResolver){
        Claims claims = getClaims(token);
        return claimsResolver.apply(claims);
    }

    private static Claims getClaims(String token){
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
    }

    public static boolean validate(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature - {}", ex.getMessage());
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token - {}", ex.getMessage());
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token - {}", ex.getMessage());
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token - {}", ex.getMessage());
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty - {}", ex.getMessage());
        }
        return false;
    }
}
