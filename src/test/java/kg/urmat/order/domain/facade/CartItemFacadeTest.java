package kg.urmat.order.domain.facade;

import kg.urmat.order.core.annotation.WithMockPrincipal;
import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.dto.cartIem.CartItemCreateRequest;
import kg.urmat.order.domain.data.dto.cartIem.CartItemUpdateRequest;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CartItemFacadeTest extends AbstractFacadeTest {

    @Autowired
    private CartItemFacade cartItemFacade;

    @Test
    @WithMockPrincipal
    void shouldAdd() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();

        var request = CartItemCreateRequest.builder()
                .quantity(5)
                .productId(1L)
                .build();
        var response = cartItemFacade.add(request, principal.getUserId(), principal.getCartId());

        assertEquals(request.quantity(), response.quantity());
        assertEquals(request.productId(), response.product().id());
    }

    @Test
    @WithMockPrincipal(role = RoleCode.ADMIN)
    void shouldNotAdd() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();

        var request = CartItemCreateRequest.builder()
                .quantity(5)
                .productId(1L)
                .build();

        assertThatThrownBy(() -> cartItemFacade.add(request, principal.getUserId(), principal.getCartId()))
                .hasMessage(String.format("No Cart found, whose User id is %d", principal.getUserId()))
                .isInstanceOf(NoSuchElementFoundException.class);
    }

    @Test
    @WithMockPrincipal
    void shouldUpdate() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();

        var createRequest = CartItemCreateRequest.builder()
                .quantity(5)
                .productId(1L)
                .build();
        var updateRequest = CartItemUpdateRequest.builder()
                .quantity(10)
                .productId(1L)
                .build();

        var createResponse = cartItemFacade.add(createRequest, principal.getUserId(), principal.getCartId());
        var updateResponse = cartItemFacade.update(updateRequest, createResponse.id(), principal.getUserId());

        assertEquals(updateRequest.quantity(), updateResponse.quantity());
        assertEquals(updateRequest.productId(), updateResponse.product().id());
    }

    @Test
    @WithMockPrincipal(role = RoleCode.ADMIN)
    void shouldNotUpdate() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();

        var request = CartItemUpdateRequest.builder()
                .quantity(10)
                .productId(1L)
                .build();

        assertThatThrownBy(() -> cartItemFacade.update(request, 1L , principal.getUserId()))
                .hasMessage(String.format("No Cart found, whose User id is %d", principal.getUserId()))
                .isInstanceOf(NoSuchElementFoundException.class);
    }
}