package kg.urmat.order.domain.facade.impl;

import kg.urmat.order.domain.data.dto.page.PageResponse;
import kg.urmat.order.domain.data.dto.page.SearchCriteria;
import kg.urmat.order.domain.data.dto.product.ProductCreateRequest;
import kg.urmat.order.domain.data.dto.product.ProductResponse;
import kg.urmat.order.domain.data.dto.product.ProductUpdateRequest;
import kg.urmat.order.domain.facade.ProductFacade;
import kg.urmat.order.domain.mapper.PageMapper;
import kg.urmat.order.domain.mapper.ProductMapper;
import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.service.ProductService;
import kg.urmat.order.domain.service.builder.SpecificationsBuilder;
import kg.urmat.order.domain.util.SearchCriteriaParserUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductFacadeImpl implements ProductFacade {

    private final ProductService productService;
    private final ProductMapper productMapper;
    private final PageMapper pageMapper;

    @Override
    public PageResponse<ProductResponse> search(int pageIndex, int size, String order, String andSearch, String orSearch) {
        List<SearchCriteria> andParams = SearchCriteriaParserUtils.parse(andSearch);
        List<SearchCriteria> orParams = SearchCriteriaParserUtils.parse(orSearch);
        List<SearchCriteria> orderParams = SearchCriteriaParserUtils.parse(order);

        SpecificationsBuilder<Product> builder = new SpecificationsBuilder<>(andParams, orParams);

        List<Sort> sorts = builder.generateSortList(orderParams);
        Sort sort = builder.andSort(sorts);
        Pageable pageable = PageRequest.of(pageIndex, size, sort);

        Page<Product> page = productService.search(builder.build(), pageable);
        List<ProductResponse> dtos = page.getContent().stream()
                .map(productMapper::toResponsePublic)
                .collect(Collectors.toList());

        return pageMapper.toResponse(page, dtos);
    }

    @Override
    public ProductResponse get(Long id) {
        return productMapper.toResponsePublic(productService.get(id));
    }

    @Override
    public ProductResponse add(ProductCreateRequest productRequest) {
        Product product = productService.save(productMapper.toEntity(productRequest));
        return productMapper.toResponsePublic(product);
    }

    @Override
    @Transactional
    public ProductResponse update(ProductUpdateRequest productRequest, Long productId) {
        Product product = productService.get(productId);
        product = productService.save(productMapper.toEntity(productRequest, product));
        return productMapper.toResponsePublic(product);
    }
}
