package kg.urmat.order.domain.data.util.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Route {

    private static final String API = "/api";

    public static final String AUTH_API = API + "/auth";

    public static final String USERS_API = API + "/users";
    public static final String PRODUCTS_API = API + "/products";
    public static final String CARTS_API = API + "/carts";
    public static final String CART_ITEMS_API = API + "/cart-items";
    public static final String ORDERS_API = API + "/orders";
    public static final String ORDER_ITEMS_API = API + "/order-items";

}
