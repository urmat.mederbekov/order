package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long> {

    boolean existsByCartIdAndProductId(Long cartId, Long productId);

}
