package kg.urmat.order.core.annotation.validator;

import kg.urmat.order.core.annotation.WithMockPrincipal;
import kg.urmat.order.domain.mapper.AuthMapper;
import kg.urmat.order.domain.data.model.Authority;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.jetbrains.annotations.NotNull;
import org.mapstruct.factory.Mappers;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.Set;

public class WithMockPrincipalSecurityContextFactory implements WithSecurityContextFactory<WithMockPrincipal> {

    private final AuthMapper authMapper = Mappers.getMapper(AuthMapper.class);

    @Override
    public SecurityContext createSecurityContext(WithMockPrincipal withMockPrincipal) {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        CustomPrincipal principal = getCustomPrincipal(withMockPrincipal);
        Authentication authentication = new UsernamePasswordAuthenticationToken(principal, principal.getPassword(), principal.getAuthorities());
        securityContext.setAuthentication(authentication);
        return securityContext;
    }

    @NotNull
    private CustomPrincipal getCustomPrincipal(WithMockPrincipal withMockPrincipal) {
        Authority authority = new Authority();
        authority.setCode(withMockPrincipal.authority());

        Role role = new Role();
        role.setCode(withMockPrincipal.role());
        role.setAuthorities(Set.of(authority));

        User user = new User();
        user.setPassword("password");
        user.setLogin(withMockPrincipal.username());
        user.setId(1L);
        user.setRoles(Set.of(role));

        if(withMockPrincipal.role().equals(RoleCode.USER)){
            Cart cart = new Cart();
            cart.setId(2L);
            user.setCart(cart);
            user.setId(2L);
        }
        return authMapper.toEntity(user);
    }
}
