package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.Order;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {

    @EntityGraph(attributePaths = "orderItems", type = EntityGraph.EntityGraphType.LOAD)
    List<Order> findAllByUserId(Long userId);
}
