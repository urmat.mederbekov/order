package kg.urmat.order.domain.facade;

import jakarta.validation.Valid;
import kg.urmat.order.domain.data.dto.page.PageResponse;
import kg.urmat.order.domain.data.dto.product.ProductCreateRequest;
import kg.urmat.order.domain.data.dto.product.ProductResponse;
import kg.urmat.order.domain.data.dto.product.ProductUpdateRequest;
import org.springframework.validation.annotation.Validated;

@Validated
public interface ProductFacade {

    PageResponse<ProductResponse> search(int pageIndex, int size, String order, String andSearch, String orSearch);

    ProductResponse get(Long id);

    ProductResponse add(@Valid ProductCreateRequest productRequest);

    ProductResponse update(@Valid ProductUpdateRequest productRequest, Long productId);
}
