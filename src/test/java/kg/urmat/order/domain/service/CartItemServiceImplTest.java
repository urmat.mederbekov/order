package kg.urmat.order.domain.service;

import jakarta.validation.ConstraintViolationException;
import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.repository.CartItemRepository;
import kg.urmat.order.domain.service.impl.CartItemServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CartItemServiceImplTest {

    @Mock
    private CartItemRepository repository;
    @InjectMocks
    private CartItemServiceImpl service;

    private final long cartId = 1L;
    private final long productId = 1L;


    @Test
    void shouldGet() {
        when(repository.findById(cartId)).thenReturn(Optional.of(new CartItem()));
        assertThat(service.get(cartId)).isNotNull();
    }

    @Test
    void shouldNotGet() {
        when(repository.findById(cartId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.get(cartId))
                .isInstanceOf(NoSuchElementFoundException.class)
                .hasMessage(String.format("No CartItem entity with id %d found", cartId));
    }

    @Test
    void shouldSave() {
        CartItem cartItem = new CartItem();

        when(repository.save(cartItem)).thenReturn(cartItem);
        assertThat(service.save(cartItem)).isNotNull();
    }

    @Test
    void shouldNotSave() {
        when(repository.save(any(CartItem.class))).thenThrow(ConstraintViolationException.class);
        assertThrows(ConstraintViolationException.class, () -> service.save(new CartItem()));
    }

    @Test
    void shouldEmptyCart() {
        List<CartItem> cartItems = List.of(new CartItem());

        service.emptyCart(cartItems);
        verify(repository, times(1)).deleteAllInBatch(cartItems);
    }

    @Test
    void shouldExistsInCart() {
        when(repository.existsByCartIdAndProductId(cartId, productId)).thenReturn(true);
        assertTrue(service.existsInCart(cartId, productId));
    }

    @Test
    void shouldNotExistsInCart() {
        when(repository.existsByCartIdAndProductId(cartId, productId)).thenReturn(false);
        assertFalse(service.existsInCart(cartId, productId));
    }
}