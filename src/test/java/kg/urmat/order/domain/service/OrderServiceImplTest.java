package kg.urmat.order.domain.service;

import jakarta.validation.ConstraintViolationException;
import kg.urmat.order.domain.data.model.Order;
import kg.urmat.order.domain.repository.OrderRepository;
import kg.urmat.order.domain.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {

    @Mock
    private OrderRepository repository;
    @InjectMocks
    private OrderServiceImpl service;

    private final long userId = 1L;

    @Disabled
    @Test
    void search() {
    }

    @Test
    void shouldGetAllByUserId() {
        when(repository.findAllByUserId(userId)).thenReturn(List.of(new Order()));
        assertEquals(1, service.getAllByUserId(userId).size());
    }

    @Test
    void shouldNotGetAllByUserId() {
        when(repository.findAllByUserId(userId)).thenReturn(List.of());
        assertEquals(0, service.getAllByUserId(userId).size());
    }

    @Test
    void shouldSave() {
        Order order = new Order();

        when(repository.save(order)).thenReturn(order);
        assertThat(service.save(order)).isNotNull();
    }

    @Test
    void shouldNotSave() {
        when(repository.save(any(Order.class))).thenThrow(ConstraintViolationException.class);
        assertThrows(ConstraintViolationException.class, () -> service.save(new Order()));
    }
}