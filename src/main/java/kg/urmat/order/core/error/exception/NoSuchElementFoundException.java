package kg.urmat.order.core.error.exception;

import java.util.NoSuchElementException;

public class NoSuchElementFoundException extends NoSuchElementException {

    public NoSuchElementFoundException(String message){
        super(message);
    }

    public NoSuchElementFoundException(String entityName, String parameter, Object value){
        super(String.format("No %s entity with %s %s found", entityName, parameter, value));
    }
}
