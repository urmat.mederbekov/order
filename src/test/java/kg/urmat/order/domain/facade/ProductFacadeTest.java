package kg.urmat.order.domain.facade;

import jakarta.validation.ConstraintViolationException;
import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.dto.product.ProductCreateRequest;
import kg.urmat.order.domain.data.dto.product.ProductUpdateRequest;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductFacadeTest extends AbstractFacadeTest {

    @Autowired
    private ProductFacade productFacade;

    @Test
    @Disabled
    void search() {
    }

    @Test
    void shouldGet() {
        var response= productFacade.get(1L);

        assertEquals(ProductCode.COMPUTER, response.code());
        assertEquals("iMac 27", response.name());
        assertEquals(1799.0, response.price().doubleValue());
        assertThat(response.description()).contains("The 27‑inch iMac is packed with powerful tools and apps that let you take any idea to the next level.");
    }

    @Test
    void shouldNotGet() {
        long productId = 1000L;
        assertThatThrownBy(() -> productFacade.get(productId))
                .hasMessage(String.format("No Product entity with id %d found", productId))
                .isInstanceOf(NoSuchElementFoundException.class);
    }

    @Test
    void shouldAdd() {
        var request = ProductCreateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(100.0)
                .description("cheap")
                .name("hp")
                .build();

        var response = productFacade.add(request);
        assertEquals(response.description(), request.description());
        assertEquals(response.price(), request.price());
        assertEquals(response.name(), request.name());
        assertEquals(response.code(), request.code());
    }

    @Test
    void shouldNotAdd() {
        var request = ProductCreateRequest.builder()
                .price(100.0)
                .description("cheap")
                .name("hp")
                .build();

        assertThatThrownBy(() -> productFacade.add(request))
                .hasMessage("add.productRequest.code: must be a valid value")
                .isInstanceOf(ConstraintViolationException.class);
    }

    @Test
    void shouldUpdate() {
        var request = ProductUpdateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(200.0)
                .description("cheap")
                .name("hp")
                .build();

        var response = productFacade.update(request, 1L);

        assertEquals(response.description(), request.description());
        assertEquals(response.price(), request.price());
        assertEquals(response.name(), request.name());
        assertEquals(response.code(), request.code());
    }

    @Test
    void shouldNotUpdate() {
        var request = ProductUpdateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(0.0)
                .description("cheap")
                .name("hp")
                .build();

        assertThatThrownBy(() -> productFacade.update(request, 1L))
                .hasMessage("update.productRequest.price: must be greater than 0")
                .isInstanceOf(ConstraintViolationException.class);
    }
}