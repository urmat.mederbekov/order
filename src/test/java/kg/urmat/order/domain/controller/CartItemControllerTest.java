package kg.urmat.order.domain.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kg.urmat.order.core.annotation.WithMockPrincipal;
import kg.urmat.order.domain.data.dto.cartIem.CartItemCreateRequest;
import kg.urmat.order.domain.data.dto.cartIem.CartItemResponse;
import kg.urmat.order.domain.data.dto.cartIem.CartItemUpdateRequest;
import kg.urmat.order.domain.data.dto.product.ProductResponse;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.facade.CartItemFacade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static kg.urmat.order.domain.data.util.constant.RoleConstant.ADMIN;
import static kg.urmat.order.domain.data.util.constant.Route.CART_ITEMS_API;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CartItemController.class)
class CartItemControllerTest extends AbstractSecurityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CartItemFacade cartItemFacade;

    private final Long cartItemId = 1L;

    @Test
    @WithMockPrincipal
    void shouldCreate() throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();
        var request = CartItemCreateRequest.builder()
                .productId(1L)
                .quantity(9)
                .build();

        var response = CartItemResponse.builder()
                .product(ProductResponse.builder().build())
                .quantity(9)
                .cartId(1L)
                .build();

        when(cartItemFacade.add(request, principal.getUserId(), principal.getCartId())).thenReturn(response);

        mockMvc.perform(post(CART_ITEMS_API)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.quantity").value(response.quantity()))
                .andExpect(jsonPath("$.cartId").value(response.cartId()))
                .andExpect(jsonPath("$.product").exists());

        verify(cartItemFacade, times(1)).add(request, principal.getUserId(), principal.getCartId());
    }
    @Test
    @WithMockUser(authorities = ADMIN)
    void shouldNotCreate() throws Exception {
        var request = CartItemCreateRequest.builder()
                .productId(1L)
                .quantity(9)
                .build();

        mockMvc.perform(post(CART_ITEMS_API)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(401));
    }

    @Test
    @WithMockPrincipal
    void update() throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();

        var request = CartItemUpdateRequest.builder()
                .quantity(12)
                .productId(2L)
                .build();

        var response = CartItemResponse.builder()
                .product(ProductResponse.builder().build())
                .quantity(12)
                .cartId(principal.getCartId())
                .build();

        when(cartItemFacade.update(request, cartItemId, principal.getUserId())).thenReturn(response);

        mockMvc.perform(put(CART_ITEMS_API + "/" + cartItemId)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.quantity").value(response.quantity()))
                .andExpect(jsonPath("$.cartId").value(response.cartId()))
                .andExpect(jsonPath("$.product").exists());

        verify(cartItemFacade, times(1)).update(request, cartItemId, principal.getUserId());
    }

    @Test
    @WithMockUser(authorities = ADMIN)
    void shouldNotUpdate() throws Exception {
        var request = CartItemUpdateRequest.builder()
                .quantity(12)
                .productId(2L)
                .build();

        mockMvc.perform(put(CART_ITEMS_API + "/" + cartItemId)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(401));
    }
}