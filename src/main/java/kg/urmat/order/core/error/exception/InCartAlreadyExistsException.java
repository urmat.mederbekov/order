package kg.urmat.order.core.error.exception;

public class InCartAlreadyExistsException extends RuntimeException {

    public InCartAlreadyExistsException(Long productId, Long cartId){
        super(String.format("Product with id %d is already exists in the cart with id %d", productId, cartId));
    }
}
