package kg.urmat.order.domain.facade;

import jakarta.validation.ConstraintViolationException;
import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.core.error.exception.UserAlreadyExistsException;
import kg.urmat.order.domain.data.dto.user.UserCreateRequest;
import kg.urmat.order.domain.data.dto.user.UserUpdateRequest;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import kg.urmat.order.domain.mapper.RoleMapper;
import kg.urmat.order.domain.service.RoleService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserFacadeTest extends AbstractFacadeTest {

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleMapper roleMapper;

    @Test
    @Disabled
    void search() {
    }

    @Test
    void shouldGet() {
        var response = userFacade.get(1L);

        assertEquals(Set.of(roleMapper.toResponsePubic(roleService.getByCode(RoleCode.ADMIN))), response.roles());
        Assertions.assertEquals("Urmat", response.name());
        Assertions.assertEquals("Mederbekov", response.surname());
        Assertions.assertEquals(LocalDate.of(2001,2,23), response.birthDate());
    }

    @Test
    void shouldNotGet() {
        long userId = 1000L;
        assertThatThrownBy(() -> userFacade.get(1000L))
                .hasMessage(String.format("No User entity with id %d found", userId))
                .isInstanceOf(NoSuchElementFoundException.class);
    }

    @Test
    void shouldAdd() {
        var request = UserCreateRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("vito2")
                .password("231231")
                .build();
        var response = userFacade.add(request);

        assertEquals(Set.of(roleMapper.toResponsePubic(roleService.getByCode(RoleCode.USER))), response.roles());
        Assertions.assertEquals(request.name(), response.name());
        Assertions.assertEquals(request.surname(), response.surname());
        Assertions.assertEquals(LocalDate.now().minusYears(18), response.birthDate());
    }

    @Test
    void shouldNotAdd() {
        var request = UserCreateRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("admin")
                .password("231231")
                .build();

        assertThatThrownBy(() -> userFacade.add(request))
                .hasMessage("User with login admin already exists")
                .isInstanceOf(UserAlreadyExistsException.class);
    }

    @Test
    void shouldUpdate() {
        var request = UserUpdateRequest.builder()
                .name("avatar")
                .surname("ang")
                .birthDate(LocalDate.now().minusYears(18))
                .roleCodes(Set.of(RoleCode.ADMIN))
                .build();
        var response = userFacade.update(request, 1L);

        assertEquals(Set.of(roleMapper.toResponsePubic(roleService.getByCode(RoleCode.ADMIN))), response.roles());
        Assertions.assertEquals(request.name(), response.name());
        Assertions.assertEquals(request.surname(), response.surname());
        Assertions.assertEquals(LocalDate.now().minusYears(18), response.birthDate());
    }

    @Test
    void shouldNotUpdate() {
        var request = UserUpdateRequest.builder()
                .surname("ang")
                .birthDate(LocalDate.now().minusYears(18))
                .roleCodes(Set.of(RoleCode.USER))
                .build();

        assertThatThrownBy(() -> userFacade.update(request, 1L))
                .hasMessage("update.userRequest.name: must not be blank")
                .isInstanceOf(ConstraintViolationException.class);

    }
}