package kg.urmat.order.domain.data.dto.auth;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;

@Builder
public record AuthRequest(
        @NotBlank
        String login,
        @NotBlank
        String password
){}