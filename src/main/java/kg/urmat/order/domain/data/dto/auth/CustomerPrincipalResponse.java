package kg.urmat.order.domain.data.dto.auth;

import kg.urmat.order.domain.data.util.enums.RoleCode;
import lombok.Builder;

import java.util.Set;

@Builder
public record CustomerPrincipalResponse(
        String name,
        String surname,
        Set<RoleCode> roleCodes,
        String token
){}