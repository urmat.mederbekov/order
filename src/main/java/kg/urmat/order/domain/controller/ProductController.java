package kg.urmat.order.domain.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import kg.urmat.order.domain.data.dto.page.PageResponse;
import kg.urmat.order.domain.data.dto.product.ProductCreateRequest;
import kg.urmat.order.domain.data.dto.product.ProductResponse;
import kg.urmat.order.domain.data.dto.product.ProductUpdateRequest;
import kg.urmat.order.domain.facade.ProductFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static kg.urmat.order.domain.data.util.constant.AuthorityConstant.READ;
import static kg.urmat.order.domain.data.util.constant.RoleConstant.ADMIN;
import static kg.urmat.order.domain.data.util.constant.Route.PRODUCTS_API;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@SecurityRequirement(name = "bearerAuth")
@RequestMapping(PRODUCTS_API)
@PreAuthorize("hasRole('" + ADMIN + "')")
@RequiredArgsConstructor
public class ProductController {

    private final ProductFacade productFacade;

    @GetMapping
    public ResponseEntity<PageResponse<ProductResponse>> search(
            @RequestParam(value = "page", defaultValue = "0") int pageIndex,
            @RequestParam(value = "size", defaultValue = "20") int size,
            @RequestParam(name = "order", required = false) String order,
            @RequestParam(name = "and-search", required = false) String andSearch,
            @RequestParam(name = "or-search", required = false) String orSearch
    ){
        return ResponseEntity.status(OK)
                .body(productFacade.search(pageIndex, size, order, andSearch, orSearch));
    }

    @PreAuthorize("hasRole('" + READ + "')")
    @GetMapping("/{id}")
    public ResponseEntity<ProductResponse> get(@PathVariable Long id){
        return ResponseEntity.status(OK).body(productFacade.get(id));
    }

    @PostMapping
    public ResponseEntity<ProductResponse> create(@RequestBody ProductCreateRequest product){
        return ResponseEntity.status(CREATED).body(productFacade.add(product));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductResponse> update(@RequestBody ProductUpdateRequest product, @PathVariable Long id){
        return ResponseEntity.status(OK).body(productFacade.update(product, id));
    }
}
