package kg.urmat.order.domain.data.dto.product;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import kg.urmat.order.core.annotation.EnumContains;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import lombok.Builder;

@Builder
public record ProductUpdateRequest(
        @EnumContains(enumClass = ProductCode.class)
        ProductCode code,
        @NotBlank
        String name,
        @NotBlank
        String description,
        @Positive
        Double price,
        @Positive
        Long quantity
){}