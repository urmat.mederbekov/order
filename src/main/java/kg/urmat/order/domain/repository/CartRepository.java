package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.Cart;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    @EntityGraph(attributePaths = {"items", "user.roles"}, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Cart> findByUserId(Long code);
}
