package kg.urmat.order.domain.service.impl;

import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.repository.UserRepository;
import kg.urmat.order.domain.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@CacheConfig(cacheNames = "users")
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Override
    public Page<User> search(Specification<User> specification, Pageable pageable) {
        log.info("Searching users");
        return repository.findAll(specification, pageable);
    }

    @Override
    @Cacheable(key = "#id")
    public User get(Long id) {
        log.info("Getting user with id {}", id);
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException("User", "id", id));
    }

    @Override
    public User getByLogin(String login) {
        log.info("Getting user with login {}", login);
        return repository.findByLogin(login)
                .orElseThrow(() -> new NoSuchElementFoundException("User", "login", login));
    }

    @Override
    @CachePut(key = "#entity.id", condition = "#entity.id == null")
    public User save(User entity) {
        log.info("Saving user {}", entity);
        return repository.save(entity);
    }

    @Override
    public boolean existsByLogin(String login) {
        log.info("Checking if user with login {} exists", login);
        return repository.existsByLogin(login);
    }
}
