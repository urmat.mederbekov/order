package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.cartIem.CartItemCreateRequest;
import kg.urmat.order.domain.data.dto.cartIem.CartItemUpdateRequest;
import kg.urmat.order.domain.data.dto.product.ProductResponse;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartItemMapperImplTest {

    @Mock
    private ProductMapper productMapper;
    @InjectMocks
    private CartItemMapper cartItemMapper = Mappers.getMapper(CartItemMapper.class);

    static CartItem cartItem;
    static Product product;
    static Cart cart;

    @BeforeAll
    static void setUp() {
        product = new Product();
        product.setCode(ProductCode.COMPUTER);
        product.setName("mac");
        product.setPrice(100);
        product.setDescription("very expensive");

        cart = new Cart();
        cart.setId(1L);

        cartItem = new CartItem();
        cartItem.setQuantity(2);
        cartItem.setCart(cart);
        cartItem.setProduct(product);
    }

    @Test
    void toResponsePublic() {
        when(productMapper.toResponsePublic(cartItem.getProduct())).thenReturn(ProductResponse.builder().build());
        var response = cartItemMapper.toResponsePublic(cartItem);

        assertEquals(cartItem.getCart().getId(), response.cartId());
        assertEquals(cartItem.getQuantity(), response.quantity());
        assertEquals(ProductResponse.builder().build(), response.product());
    }

    @Test
    void toEntityFromCreate() {
        var request = CartItemCreateRequest.builder()
                .productId(1L)
                .quantity(9)
                .build();

        var createdCartItem = cartItemMapper.toEntity(request, product, cart);

        assertEquals(request.quantity(), createdCartItem.getQuantity());
        assertEquals(cart, createdCartItem.getCart());
        assertEquals(product, createdCartItem.getProduct());
    }

    @Test
    void toEntityFromUpdate() {
        var request = CartItemUpdateRequest.builder()
                .quantity(12)
                .productId(2L)
                .build();

        var updatedCartItem = cartItemMapper.toEntity(request, cartItem, product, cart);

        assertEquals(request.quantity(), updatedCartItem.getQuantity());
        assertEquals(cart, updatedCartItem.getCart());
        assertEquals(product, updatedCartItem.getProduct());

    }
}