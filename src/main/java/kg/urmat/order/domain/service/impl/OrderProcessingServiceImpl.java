package kg.urmat.order.domain.service.impl;

import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.Order;
import kg.urmat.order.domain.data.model.OrderItem;
import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.mapper.OrderMapper;
import kg.urmat.order.domain.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "kafka.enabled", havingValue = "true", matchIfMissing = true)
public class OrderProcessingServiceImpl implements OrderProcessingService {

    private final OrderService orderService;
    private final ProductService productService;
    private final CartItemService cartItemService;
    private final CartService cartService;
    private final OrderMapper orderMapper;

    @Transactional
    @KafkaListener(topics = "${kafka.topic.name}", groupId = "${kafka.topic.group_id}")
    public void orderListener(Long userId) {
        log.info("Received order for user {}", userId);
        Cart cart = cartService.getByUserId(userId);
        Order order = orderMapper.toEntity(cart);
        List<OrderItem> orderItems = order.getOrderItems();

        Map<Long, Product> productMap = getProductMap(orderItems);

        if (!areAllProductsInStock(productMap, orderItems)) {
            log.info("Not all products are in stock");
            return;
        }

        order.setTotalCost(calculateTotalCost(productMap, orderItems));
        saveOrderAndEmptyCart(order, cart);
    }

    private Map<Long, Product> getProductMap(List<OrderItem> orderItems) {
        return productService.getAllById(orderItems.stream()
                        .map(orderItem -> orderItem.getProduct().getId())
                        .collect(Collectors.toList()))
                .stream()
                .collect(Collectors.toMap(Product::getId, product -> product));
    }

    private boolean areAllProductsInStock(Map<Long, Product> productMap, List<OrderItem> orderItems) {
        log.info("Checking if all products are in stock");
        for (OrderItem orderItem : orderItems) {
            Product product = productMap.get(orderItem.getProduct().getId());
            if (orderItem.getQuantity() > product.getQuantity()) {
                return false;
            }
        }
        return true;
    }
    private BigDecimal calculateTotalCost(Map<Long, Product> productMap, List<OrderItem> orderItems) {
        log.info("Calculating total cost");
        BigDecimal totalCost = BigDecimal.ZERO;
        for (OrderItem orderItem : orderItems) {
            Product product = productMap.get(orderItem.getProduct().getId());
            product.setQuantity(product.getQuantity() - orderItem.getQuantity());
            totalCost = totalCost.add(BigDecimal.valueOf(product.getPrice() * orderItem.getQuantity()));
        }
        return totalCost;
    }

    private void saveOrderAndEmptyCart(Order order, Cart cart) {
        log.info("Saving order {}", order);
        orderService.save(order);
        log.info("Emptying cart");
        cartItemService.emptyCart(cart.getItems());
    }
}