package kg.urmat.order.domain.util;

import kg.urmat.order.domain.data.dto.page.SearchCriteria;
import kg.urmat.order.domain.data.util.enums.SearchOperation;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class SearchCriteriaParserUtils {

    private static final String wordRegex = "[a-zA-Z]\\w*";
    private static final String valueRegex = "\\w+";
    private static final String operatorRegex = "(:|<|>|!|\\+|-|\\s)";
    private static final String timestampRegex = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0 -9]{2}:[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}";
    private static final String idRegex = "\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}";
    private static final String fullRegex = "(" + wordRegex + ")" + operatorRegex + "(" + timestampRegex + "|" + idRegex + "|" + valueRegex + ")?,";
    private static final Pattern searchPattern = Pattern.compile(fullRegex);

    public static List<SearchCriteria> parse(String searchString) {
        return Optional.ofNullable(searchString)
                .map(s -> searchPattern.matcher(searchString + ",").results()
                        .map(matchResult -> new SearchCriteria(
                                matchResult.group(1),
                                SearchOperation.getSimpleOperation(matchResult.group(2)),
                                matchResult.group(3)
                                )
                        ).collect(Collectors.toList())
                ).orElse(Collections.emptyList());
    }
}
