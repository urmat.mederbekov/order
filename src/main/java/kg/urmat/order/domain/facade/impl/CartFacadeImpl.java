package kg.urmat.order.domain.facade.impl;

import kg.urmat.order.domain.data.dto.cart.CartResponse;
import kg.urmat.order.domain.facade.CartFacade;
import kg.urmat.order.domain.mapper.CartMapper;
import kg.urmat.order.domain.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CartFacadeImpl implements CartFacade {

    private final CartService cartService;
    private final CartMapper cartMapper;

    @Override
    public CartResponse getByUserId(Long userId) {
        return cartMapper.toResponsePublic(cartService.getByUserId(userId));
    }
}
