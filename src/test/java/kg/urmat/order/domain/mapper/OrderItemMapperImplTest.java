package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.product.ProductResponse;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.data.model.Order;
import kg.urmat.order.domain.data.model.OrderItem;
import kg.urmat.order.domain.data.model.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderItemMapperImplTest {

    @Mock
    private ProductMapper productMapper;
    @InjectMocks
    private OrderItemMapper orderItemMapper = Mappers.getMapper(OrderItemMapper.class);

    @Test
    void toResponsePublic() {
        Order order = new Order();
        order.setId(1L);

        OrderItem orderItem = new OrderItem();
        orderItem.setOrder(order);
        orderItem.setQuantity(7);
        orderItem.setProduct(new Product());

        when(productMapper.toResponsePublic(orderItem.getProduct())).thenReturn(ProductResponse.builder().build());
        var response = orderItemMapper.toResponsePublic(orderItem);

        assertEquals(orderItem.getOrder().getId(), response.orderId());
        assertEquals(orderItem.getQuantity(), response.quantity());
        assertEquals(ProductResponse.builder().build(), response.product());
    }

    @Test
    void toEntities() {
        Product product1 = new Product();
        product1.setName("mac");
        CartItem cartItem1 = new CartItem();
        cartItem1.setId(1L);
        cartItem1.setProduct(product1);
        cartItem1.setQuantity(8);

        Product product2 = new Product();
        product2.setName("hp");
        CartItem cartItem2 = new CartItem();
        cartItem2.setId(2L);
        cartItem2.setProduct(product2);
        cartItem2.setQuantity(6);

        List<CartItem> cartItems = List.of(cartItem1, cartItem2);

        var orderItems = orderItemMapper.toEntities(cartItems);
        for (int i = 0; i < orderItems.size(); i++) {
            assertNull(orderItems.get(i).getId());
            assertEquals(cartItems.get(i).getQuantity(), orderItems.get(i).getQuantity());
            assertEquals(cartItems.get(i).getProduct().getName(), orderItems.get(i).getProduct().getName());
        }


    }
}