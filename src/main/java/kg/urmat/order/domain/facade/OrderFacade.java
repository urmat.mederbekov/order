package kg.urmat.order.domain.facade;

import kg.urmat.order.domain.data.dto.order.OrderResponse;
import kg.urmat.order.domain.data.dto.page.PageResponse;

import java.util.List;

public interface OrderFacade {

    PageResponse<OrderResponse> search(int pageIndex, int size, String order, String andSearch, String orSearch);

    void add(Long userId);

    List<OrderResponse> getAllByUserId(Long userId);
}
