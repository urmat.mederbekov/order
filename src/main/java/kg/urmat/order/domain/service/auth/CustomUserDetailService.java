package kg.urmat.order.domain.service.auth;

import kg.urmat.order.domain.mapper.AuthMapper;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {

    private final UserService userService;
    private final AuthMapper authMapper;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        log.info("Loading user with login {}", login);
        User user = userService.getByLogin(login);
        return authMapper.toEntity(user);
    }

}
