package kg.urmat.order.core.error.response;

import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

public record ApiError(
        HttpStatus status,
        String message,
        List<String> errors
) {
    public ApiError(HttpStatus status, List<String> errors) {
        this(status, status.getReasonPhrase(), errors);
    }

    public ApiError(HttpStatus status, String error) {
        this(status, status.getReasonPhrase(), Collections.singletonList(error));
    }
}
