package kg.urmat.order.domain.service;

import jakarta.validation.ConstraintViolationException;
import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.repository.CartRepository;
import kg.urmat.order.domain.service.impl.CartServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartServiceImplTest {
    
    @Mock
    private CartRepository repository;
    @InjectMocks
    private CartServiceImpl service;

    private final long orderId = 1L;
    private final long userId = 1L;

    @Test
    void shouldGet() {
        when(repository.findById(orderId)).thenReturn(Optional.of(new Cart()));
        assertThat(service.get(orderId)).isNotNull();
    }

    @Test
    void shouldNotGet() {
        when(repository.findById(orderId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.get(orderId))
                .isInstanceOf(NoSuchElementFoundException.class)
                .hasMessage(String.format("No Cart entity with id %d found", orderId));
    }

    @Test
    void shouldGetByUserId() {
        when(repository.findByUserId(userId)).thenReturn(Optional.of(new Cart()));
        assertThat(service.getByUserId(userId)).isNotNull();
    }

    @Test
    void shouldNotGetByUserId() {
        when(repository.findByUserId(orderId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.getByUserId(userId))
                .isInstanceOf(NoSuchElementFoundException.class)
                .hasMessage(String.format("No Cart found, whose User id is %d", userId));
    }

    @Test
    void shouldSave() {
        Cart cart = new Cart();

        when(repository.save(cart)).thenReturn(cart);
        assertThat(service.save(cart)).isNotNull();
    }

    @Test
    void shouldNotSave() {
        when(repository.save(any(Cart.class))).thenThrow(ConstraintViolationException.class);
        assertThrows(ConstraintViolationException.class, () -> service.save(new Cart()));
    }
}