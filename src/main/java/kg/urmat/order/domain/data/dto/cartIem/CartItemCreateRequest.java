package kg.urmat.order.domain.data.dto.cartIem;

import jakarta.validation.constraints.Positive;
import lombok.Builder;

@Builder
public record CartItemCreateRequest(
        @Positive
        Long productId,
        @Positive
        int quantity
){}