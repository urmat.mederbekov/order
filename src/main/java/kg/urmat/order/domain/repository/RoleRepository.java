package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    @EntityGraph(attributePaths = {"authorities"}, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Role> findByCode(RoleCode code);

    Set<Role> findByCodeIn(Collection<RoleCode> codes);

}
