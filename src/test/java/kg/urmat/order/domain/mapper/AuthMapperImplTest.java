package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuthMapperImplTest {

    private final AuthMapper authMapper = Mappers.getMapper(AuthMapper.class);

    @Test
    void toResponsePrivate() {
        var request = new CustomPrincipal();
        request.setName("name");
        request.setSurname("surname");
        request.setRoleCodes(Set.of(RoleCode.USER));
        String token = "token";

        var response = authMapper.toResponsePrivate(request, token);

        assertEquals(token, response.token());
        assertEquals(request.getName(), response.name());
        assertEquals(request.getSurname(), response.surname());
        assertEquals(request.getRoleCodes(), response.roleCodes());

    }
}