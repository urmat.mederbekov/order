package kg.urmat.order.domain.service;

import kg.urmat.order.domain.data.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface OrderService {

    Page<Order> search(Specification<Order> build, Pageable pageable);

    List<Order> getAllByUserId(Long userId);

    Order save(Order entity);
}
