package kg.urmat.order.domain.facade;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderFacadeTest extends AbstractFacadeTest {

    @Autowired
    private OrderFacade orderFacade;

    @Test
    @Disabled
    void search() {
    }

    @Test
    @Disabled("add() method is void")
    void add() {
        orderFacade.add(2L);
    }

    @Test
    void shouldGetAllByUserId() {
        var response = orderFacade.getAllByUserId(2L);
        assertEquals(0, response.size());
    }
}