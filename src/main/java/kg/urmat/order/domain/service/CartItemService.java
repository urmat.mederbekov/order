package kg.urmat.order.domain.service;

import kg.urmat.order.domain.data.model.CartItem;

import java.util.List;

public interface CartItemService {

    CartItem get(Long id);

    CartItem save(CartItem entity);

    void emptyCart(List<CartItem> entities);

    boolean existsInCart(Long cartId, Long productId);
}
