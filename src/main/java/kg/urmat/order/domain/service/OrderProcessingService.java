package kg.urmat.order.domain.service;

public interface OrderProcessingService {
    void orderListener(Long userId);
}
