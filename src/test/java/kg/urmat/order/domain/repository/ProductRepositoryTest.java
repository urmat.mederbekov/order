package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ProductRepositoryTest {

    @Autowired
    private ProductRepository repository;

    static Product product;

    @BeforeAll
    static void setUp() {
        product = new Product();
        product.setCode(ProductCode.COMPUTER);
        product.setName("mac");
        product.setPrice(100);
        product.setDescription("very expensive");
    }

    @Test
    void shouldFindAllByCode() {
        repository.save(product);
        assertTrue(repository.findAllByCode(ProductCode.COMPUTER).size() > 1);
    }

    @Test
    void shouldNotFindAllByCode() {
        assertFalse(repository.findAllByCode(ProductCode.IPAD).size() > 1);
    }
}