package kg.urmat.order.domain.data.model;

import jakarta.persistence.*;
import kg.urmat.order.domain.data.model.base.BaseEntity;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "roles")
@NamedEntityGraph(name = "role.authorities", attributeNodes = @NamedAttributeNode("authorities"))
public class Role extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 7422269731992737879L;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, unique = true)
    private RoleCode code;

    @Column(nullable = false)
    private String name;

    @ManyToMany
    @JoinTable(name ="m2m_role_authority",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Set<Authority> authorities;
}
