package kg.urmat.order.core.config.security;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.service.auth.CustomUserDetailService;
import kg.urmat.order.domain.util.JwtTokenUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;


@Component
@RequiredArgsConstructor
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final CustomUserDetailService userDetailService;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws ServletException, IOException {

        final String header = request.getHeader(AUTHORIZATION);
        if (isNull(header) || !header.startsWith("Bearer ")){
            chain.doFilter(request, response);
            return;
        }

        final String token = header.split(" ")[1].trim();
        if(!JwtTokenUtils.validate(token)){
            chain.doFilter(request, response);
            return;
        }

        CustomPrincipal principal = (CustomPrincipal) userDetailService.loadUserByUsername(JwtTokenUtils.getUsername(token));

        UsernamePasswordAuthenticationToken authentication =new UsernamePasswordAuthenticationToken(
                principal, null, principal.getAuthorities()
        );

        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }
}
