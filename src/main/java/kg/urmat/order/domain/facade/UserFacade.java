package kg.urmat.order.domain.facade;

import jakarta.validation.Valid;
import kg.urmat.order.domain.data.dto.page.PageResponse;
import kg.urmat.order.domain.data.dto.user.UserCreateRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.dto.user.UserUpdateRequest;
import org.springframework.validation.annotation.Validated;

@Validated
public interface UserFacade {

    PageResponse<UserResponse> search(int pageIndex, int size, String order, String andSearch, String orSearch);

    UserResponse get(Long id);

    UserResponse add(@Valid UserCreateRequest userRequest);

    UserResponse update(@Valid UserUpdateRequest userRequest, Long userId);

}
