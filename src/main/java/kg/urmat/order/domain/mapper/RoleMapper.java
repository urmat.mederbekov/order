package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.role.RoleResponse;
import kg.urmat.order.domain.data.model.Authority;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.util.enums.AuthorityCode;
import org.mapstruct.Mapper;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleResponse toResponsePubic(Role role);

    default Set<AuthorityCode> mapAuthorities(Set<Authority> authorities) {
        return authorities.stream()
                .map(Authority::getCode)
                .collect(Collectors.toSet());
    }
}
