package kg.urmat.order.domain.controller;

import jakarta.validation.Valid;
import kg.urmat.order.domain.data.dto.auth.AuthRequest;
import kg.urmat.order.domain.data.dto.auth.CustomerPrincipalResponse;
import kg.urmat.order.domain.data.dto.user.UserRegisterRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.facade.AuthFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static kg.urmat.order.domain.data.util.constant.Route.AUTH_API;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(AUTH_API)
@RequiredArgsConstructor
public class AuthController {

    private final AuthFacade authFacade;

    @PostMapping("/login")
    public ResponseEntity<CustomerPrincipalResponse> login(@Valid @RequestBody AuthRequest loginRequest){
        CustomerPrincipalResponse userResponse = authFacade.login(loginRequest);
        return ResponseEntity.status(OK)
                .header(AUTHORIZATION, userResponse.token())
                .body(userResponse);
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponse> register(@Valid @RequestBody UserRegisterRequest userRequest){
        return ResponseEntity.status(CREATED).body(authFacade.register(userRequest));
    }
}
