package kg.urmat.order.domain.facade.impl;

import kg.urmat.order.domain.data.dto.page.PageResponse;
import kg.urmat.order.domain.data.dto.page.SearchCriteria;
import kg.urmat.order.domain.data.dto.user.UserCreateRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.dto.user.UserUpdateRequest;
import kg.urmat.order.core.error.exception.UserAlreadyExistsException;
import kg.urmat.order.domain.facade.UserFacade;
import kg.urmat.order.domain.mapper.CartMapper;
import kg.urmat.order.domain.mapper.PageMapper;
import kg.urmat.order.domain.mapper.UserMapper;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.service.CartService;
import kg.urmat.order.domain.service.RoleService;
import kg.urmat.order.domain.service.UserService;
import kg.urmat.order.domain.service.builder.SpecificationsBuilder;
import kg.urmat.order.domain.util.SearchCriteriaParserUtils;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class UserFacadeImpl implements UserFacade {

    private final UserService userService;
    private final RoleService roleService;
    private final CartService cartService;
    private final PasswordEncoder encoder;
    private final UserMapper userMapper;
    private final PageMapper pageMapper;
    private final CartMapper cartMapper;

    @Override
    public PageResponse<UserResponse> search(int pageIndex, int size, String order, String andSearch, String orSearch) {
        List<SearchCriteria> andParams = SearchCriteriaParserUtils.parse(andSearch);
        List<SearchCriteria> orParams = SearchCriteriaParserUtils.parse(orSearch);
        List<SearchCriteria> orderParams = SearchCriteriaParserUtils.parse(order);

        SpecificationsBuilder<User> builder = new SpecificationsBuilder<>(andParams, orParams);

        List<Sort> sorts = builder.generateSortList(orderParams);
        Sort sort = builder.andSort(sorts);
        Pageable pageable = PageRequest.of(pageIndex, size, sort);

        Page<User> page = userService.search(builder.build(), pageable);
        List<UserResponse> dtos = page.getContent().stream()
                .map(userMapper::toResponsePublic)
                .collect(Collectors.toList());

        return pageMapper.toResponse(page, dtos);

    }

    @Override
    public UserResponse get(Long id) {
        return userMapper.toResponsePublic(userService.get(id));
    }

    @Override
    @Transactional
    public UserResponse add(UserCreateRequest userRequest) {
        if (userService.existsByLogin(userRequest.login()))
            throw new UserAlreadyExistsException(userRequest.login());
        String password = encoder.encode(userRequest.password());
        Set<Role> roles = isNull(userRequest.roleCodes()) || userRequest.roleCodes().isEmpty() ?
                new HashSet<>(Collections.singleton(roleService.getByCode(RoleCode.USER))) :
                roleService.getAllByCodes(userRequest.roleCodes());
        User user = userService.save(userMapper.toEntity(userRequest, password, roles));
        if (user.getRoles().stream().map(Role::getCode).anyMatch(code -> code.equals(RoleCode.USER))){
            cartService.save(cartMapper.toEntity(user));
        }
        return userMapper.toResponsePublic(user);
    }

    @Override
    @Transactional
    public UserResponse update(UserUpdateRequest userRequest, Long id) {
        User user = userService.get(id);
        Set<Role> roles = isNull(userRequest.roleCodes()) || userRequest.roleCodes().isEmpty() ?
                new HashSet<>(Collections.singleton(roleService.getByCode(RoleCode.USER))) :
                roleService.getAllByCodes(userRequest.roleCodes());
        user = userService.save(userMapper.toEntity(userRequest, user, roles));
        if (user.getRoles().stream().map(Role::getCode).anyMatch(code -> code.equals(RoleCode.USER))){
            cartService.save(cartMapper.toEntity(user));
        }
        return userMapper.toResponsePublic(user);
    }
}
