package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.orderItem.OrderItemResponse;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.data.model.OrderItem;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring", uses = ProductMapper.class)
public interface OrderItemMapper {

    @Mapping(target = "orderId", source = "order.id")
    OrderItemResponse toResponsePublic(OrderItem orderItem);

    @IterableMapping(qualifiedByName = "mapWithoutId")
    List<OrderItem> toEntities(List<CartItem> cartItems);

    @Named("mapWithoutId")
    @Mapping(target = "id", ignore = true)
    OrderItem mapWithoutId(CartItem source);
}
