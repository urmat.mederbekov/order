package kg.urmat.order.domain.data.model;

import jakarta.persistence.*;
import kg.urmat.order.domain.data.model.base.BaseEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "m2m_order_items")
public class OrderItem extends BaseEntity {

    @Serial
    private static final long serialVersionUID = -4104148297539595247L;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "product_id")
    private Product product;

    private int quantity;
}
