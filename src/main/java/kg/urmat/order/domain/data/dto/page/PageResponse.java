package kg.urmat.order.domain.data.dto.page;

import java.util.List;

public record PageResponse<D>(
        Integer page,
        Integer size,
        Integer totalPages,
        Long totalElements,
        List<D> content
){}
