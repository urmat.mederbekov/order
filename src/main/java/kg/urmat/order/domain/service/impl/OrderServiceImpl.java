package kg.urmat.order.domain.service.impl;

import kg.urmat.order.domain.data.model.Order;
import kg.urmat.order.domain.repository.OrderRepository;
import kg.urmat.order.domain.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;

    @Override
    public Page<Order> search(Specification<Order> build, Pageable pageable) {
        log.info("Searching orders");
        return repository.findAll(build, pageable);
    }

    @Override
    public List<Order> getAllByUserId(Long userId) {
        log.info("Getting all orders by user id {}", userId);
        return repository.findAllByUserId(userId);
    }

    @Override
    public Order save(Order entity) {
        log.info("Saving order {}", entity);
        return repository.save(entity);
    }
}
