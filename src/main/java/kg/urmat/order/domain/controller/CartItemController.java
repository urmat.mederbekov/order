package kg.urmat.order.domain.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import kg.urmat.order.domain.data.dto.cartIem.CartItemCreateRequest;
import kg.urmat.order.domain.data.dto.cartIem.CartItemResponse;
import kg.urmat.order.domain.data.dto.cartIem.CartItemUpdateRequest;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.facade.CartItemFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import static kg.urmat.order.domain.data.util.constant.RoleConstant.USER;
import static kg.urmat.order.domain.data.util.constant.Route.CART_ITEMS_API;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@SecurityRequirement(name = "bearerAuth")
@RequestMapping(CART_ITEMS_API)
@PreAuthorize("hasRole('" + USER + "')")
@RequiredArgsConstructor
public class CartItemController {

    private final CartItemFacade cartItemFacade;

    @PostMapping
    public ResponseEntity<CartItemResponse> create(
            @RequestBody CartItemCreateRequest cartItem,
            Authentication authentication
    ){
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();
        return ResponseEntity.status(CREATED)
                .body(cartItemFacade.add(cartItem, principal.getUserId(), principal.getCartId()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<CartItemResponse> update(
            @RequestBody CartItemUpdateRequest cartItem,
            @PathVariable Long id, Authentication authentication
    ){
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();
        return ResponseEntity.status(OK)
                .body(cartItemFacade.update(cartItem, id, principal.getUserId()));
    }
}
