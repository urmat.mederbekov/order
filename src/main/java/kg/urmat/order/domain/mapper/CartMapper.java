package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.cart.CartResponse;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CartItemMapper.class, UserMapper.class})
public interface CartMapper {

    CartResponse toResponsePublic(Cart entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    Cart toEntity(User user);
}
