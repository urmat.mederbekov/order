package kg.urmat.order.domain.facade;

import kg.urmat.order.domain.data.dto.cart.CartResponse;

public interface CartFacade {

    CartResponse getByUserId(Long userId);
}
