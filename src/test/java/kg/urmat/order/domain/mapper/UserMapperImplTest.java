package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.role.RoleResponse;
import kg.urmat.order.domain.data.dto.user.UserCreateRequest;
import kg.urmat.order.domain.data.dto.user.UserRegisterRequest;
import kg.urmat.order.domain.data.dto.user.UserUpdateRequest;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserMapperImplTest {

    @Mock
    private RoleMapper roleMapper;
    @InjectMocks
    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    static User user;
    static Set<Role> roles;

    @BeforeAll
    static void setUp() {
        user = new User();
        user.setLogin("admin");
        user.setName("joe");
        user.setSurname("johnson");
        user.setPassword("0000");
        user.setBirthDate(LocalDate.now().minusYears(18));
        user.setRoles(Set.of(new Role()));
        roles = Set.of(new Role());
    }

    @Test
    void toResponsePublic() {
        when(roleMapper.toResponsePubic(any(Role.class))).thenReturn(RoleResponse.builder().build());
        var response = userMapper.toResponsePublic(user);

        assertEquals(user.getName(), response.name());
        assertEquals(user.getSurname(), response.surname());
        assertEquals(user.getBirthDate(), response.birthDate());
        assertEquals(Set.of(RoleResponse.builder().build()), response.roles());
    }

    @Test
    void toEntityFromRegister() {
        var request = UserRegisterRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("vito1")
                .password("asdada")
                .build();

        var user = userMapper.toEntity(request, request.password(), roles);

        assertEquals(request.login(), user.getLogin());
        assertEquals(request.password(), user.getPassword());
        assertEquals(request.name(), user.getName());
        assertEquals(request.surname(), user.getSurname());
        assertEquals(request.birthDate(), user.getBirthDate());
        assertEquals(roles, user.getRoles());
    }

    @Test
    void toEntityFromCreate() {
        var request = UserCreateRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("vito1")
                .password("231231")
                .build();

        var user = userMapper.toEntity(request, request.password(), roles);

        assertEquals(request.login(), user.getLogin());
        assertEquals(request.password(), user.getPassword());
        assertEquals(request.name(), user.getName());
        assertEquals(request.surname(), user.getSurname());
        assertEquals(request.birthDate(), user.getBirthDate());
        assertEquals(roles, user.getRoles());
    }

    @Test
    void toEntityFromUpdate() {
        var request = UserUpdateRequest.builder()
                .name("avatar")
                .surname("ang")
                .birthDate(LocalDate.now().minusYears(18))
                .roleCodes(Set.of(RoleCode.USER))
                .build();

        var updatedUser = userMapper.toEntity(request, user, roles);

        assertEquals(request.name(), updatedUser.getName());
        assertEquals(request.surname(), updatedUser.getSurname());
        assertEquals(request.birthDate(), updatedUser.getBirthDate());
        assertEquals(roles, updatedUser.getRoles());
    }
}