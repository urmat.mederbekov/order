package kg.urmat.order.domain.facade;

import kg.urmat.order.core.error.exception.UserAlreadyExistsException;
import kg.urmat.order.domain.data.dto.auth.AuthRequest;
import kg.urmat.order.domain.data.dto.role.RoleResponse;
import kg.urmat.order.domain.data.dto.user.UserRegisterRequest;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;


class AuthFacadeTest extends AbstractFacadeTest{

    @Autowired
    private AuthFacade authFacade;

    @Test
    void shouldLogin() {
        var request = AuthRequest.builder()
                .login("admin")
                .password("123")
                .build();
        var response = authFacade.login(request);

        assertThat(response.token()).isNotBlank();
    }

    @Test
    void shouldNotLogin() {
        var request = AuthRequest.builder()
                .login("admin")
                .password("1234")
                .build();

        assertThatThrownBy(() -> authFacade.login(request))
                .hasMessage("Bad credentials")
                .isInstanceOf(BadCredentialsException.class);
    }

    @Test
    void shouldRegister() {
        var request = UserRegisterRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("vito1")
                .password("231231")
                .build();
        var response = authFacade.register(request);

        assertEquals(request.name(), response.name());
        assertEquals(request.surname(), response.surname());
        assertEquals(request.birthDate(), response.birthDate());
        assertEquals(Set.of(RoleCode.USER),
                response.roles().stream()
                        .map(RoleResponse::code)
                        .collect(Collectors.toSet())
        );
    }

    @Test
    void shouldNotRegister() {
        var request = UserRegisterRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("user")
                .password("231231")
                .build();

        assertThatThrownBy(() -> authFacade.register(request))
                .hasMessage("User with login user already exists")
                .isInstanceOf(UserAlreadyExistsException.class);
    }
}