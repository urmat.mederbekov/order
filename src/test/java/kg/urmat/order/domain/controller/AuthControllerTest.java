package kg.urmat.order.domain.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kg.urmat.order.domain.data.dto.auth.AuthRequest;
import kg.urmat.order.domain.data.dto.auth.CustomerPrincipalResponse;
import kg.urmat.order.domain.data.dto.role.RoleResponse;
import kg.urmat.order.domain.data.dto.user.UserRegisterRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import kg.urmat.order.domain.facade.AuthFacade;
import kg.urmat.order.domain.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Set;

import static kg.urmat.order.domain.data.util.constant.Route.AUTH_API;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AuthController.class)
class AuthControllerTest extends AbstractSecurityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AuthFacade authFacade;

    @MockBean
    private UserService userService;

    @Test
    void login() throws Exception {

        var auth = AuthRequest.builder()
                .login("user")
                .password("123")
                .build();

        var response = CustomerPrincipalResponse.builder()
                .name("john")
                .surname("doe")
                .roleCodes(Set.of(RoleCode.USER))
                .token("encrypted 123")
                .build();

        when(authFacade.login(auth)).thenReturn(response);

        mockMvc.perform(post(AUTH_API + "/login")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(auth)))
                .andExpect(status().is(200))
                .andExpect(header().exists(AUTHORIZATION))
                .andExpect(jsonPath("$.name").value(response.name()))
                .andExpect(jsonPath("$.surname").value(response.surname()))
                .andExpect(jsonPath("$.roleCodes").value(RoleCode.USER.name()))
                .andExpect(jsonPath("$.token").value(response.token()));

        verify(authFacade, times(1)).login(auth);
    }

    @Test
    void shouldRegister() throws Exception {
        var request = UserRegisterRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("vito1")
                .password("231231")
                .build();

        var response = UserResponse.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .roles(Set.of(RoleResponse.builder().build()))
                .build();

        when(authFacade.register(request)).thenReturn(response);

        mockMvc.perform(post(AUTH_API + "/register")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.name").value(response.name()))
                .andExpect(jsonPath("$.surname").value(response.surname()))
                .andExpect(jsonPath("$.birthDate").value(response.birthDate().toString()))
                .andExpect(jsonPath("$.roles").exists());

        verify(authFacade, times(1)).register(request);
    }

    @Test
    void shouldNotRegister() throws Exception {
        var request = UserRegisterRequest.builder()
                .birthDate(LocalDate.now().plusDays(1))
                .name("vito")
                .surname("scaletta")
                .login("vito1")
                .password("231231")
                .build();

        when(userService.existsByLogin(request.login())).thenReturn(true);

        mockMvc.perform(post(AUTH_API + "/register")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(400));

        verify(authFacade, times(0)).register(request);
    }
}