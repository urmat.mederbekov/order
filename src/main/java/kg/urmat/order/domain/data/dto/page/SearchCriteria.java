package kg.urmat.order.domain.data.dto.page;

import kg.urmat.order.domain.data.util.enums.SearchOperation;

public record SearchCriteria(
        String field,
        SearchOperation operation,
        Object value
) {}