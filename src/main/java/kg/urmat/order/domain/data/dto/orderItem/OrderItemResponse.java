package kg.urmat.order.domain.data.dto.orderItem;

import kg.urmat.order.domain.data.dto.product.ProductResponse;
import lombok.Builder;

@Builder
public record OrderItemResponse(
        Long id,
        Long orderId,
        ProductResponse product,
        int quantity
){}