package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.orderItem.OrderItemResponse;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.model.*;
import kg.urmat.order.domain.data.util.enums.StatusCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderMapperImplTest {

    @Mock
    private OrderItemMapper orderItemMapper;
    @Mock
    private UserMapper userMapper;
    @InjectMocks
    private OrderMapper orderMapper = Mappers.getMapper(OrderMapper.class);

    @Test
    void toResponsePublic() {
        Order order = new Order();
        order.setStatus(StatusCode.PROCESSING);
        order.setTotalCost(BigDecimal.valueOf(500));
        order.setUser(new User());
        order.setOrderItems(List.of(new OrderItem()));

        when(orderItemMapper.toResponsePublic(new OrderItem())).thenReturn(OrderItemResponse.builder().build());
        when(userMapper.toResponsePublic(new User())).thenReturn(UserResponse.builder().build());
        var response = orderMapper.toResponsePublic(order);

        assertEquals(UserResponse.builder().build(), response.user());
        assertEquals(List.of(OrderItemResponse.builder().build()), response.orderItems());
        assertEquals(order.getStatus(), response.status());
        assertEquals(order.getTotalCost(), response.totalCost());
    }

    @Test
    void toEntity() {
        Cart cart = new Cart();
        cart.setUser(new User());
        cart.setItems(List.of(new CartItem()));

        Product product1 = new Product();
        product1.setPrice(5);

        OrderItem orderItem1 = new OrderItem();
        orderItem1.setProduct(product1);
        orderItem1.setQuantity(5);

        when(orderItemMapper.toEntities(cart.getItems())).thenReturn(List.of(orderItem1));
        var order = orderMapper.toEntity(cart);

        assertEquals(cart.getUser(), order.getUser());
        assertEquals(List.of(orderItem1), order.getOrderItems());
    }
}