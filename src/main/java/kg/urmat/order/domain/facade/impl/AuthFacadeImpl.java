package kg.urmat.order.domain.facade.impl;

import kg.urmat.order.domain.util.JwtTokenUtils;
import kg.urmat.order.domain.data.dto.auth.AuthRequest;
import kg.urmat.order.domain.data.dto.auth.CustomerPrincipalResponse;
import kg.urmat.order.domain.data.dto.user.UserRegisterRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.core.error.exception.UserAlreadyExistsException;
import kg.urmat.order.domain.facade.AuthFacade;
import kg.urmat.order.domain.mapper.AuthMapper;
import kg.urmat.order.domain.mapper.CartMapper;
import kg.urmat.order.domain.mapper.UserMapper;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.service.CartService;
import kg.urmat.order.domain.service.RoleService;
import kg.urmat.order.domain.service.UserService;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

@Service
@RequiredArgsConstructor
public class AuthFacadeImpl implements AuthFacade {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final RoleService roleService;
    private final CartService cartService;
    private final PasswordEncoder encoder;
    private final UserMapper userMapper;
    private final AuthMapper authMapper;
    private final CartMapper cartMapper;

    @Override
    public CustomerPrincipalResponse login(AuthRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.login(), loginRequest.password(), new ArrayList<>())
        );
        CustomPrincipal user = (CustomPrincipal) authentication.getPrincipal();
        return authMapper.toResponsePrivate(user, JwtTokenUtils.generateToken(user));
    }

    @Override
    @Transactional
    public UserResponse register(UserRegisterRequest userRequest) {
        if (userService.existsByLogin(userRequest.login()))
            throw new UserAlreadyExistsException(userRequest.login());
        String password = encoder.encode(userRequest.password());
        HashSet<Role> roles = new HashSet<>(Collections.singleton(roleService.getByCode(RoleCode.USER)));
        User user = userService.save(userMapper.toEntity(userRequest, password, roles));
        if (user.getRoles().stream().map(Role::getCode).anyMatch(code -> code.equals(RoleCode.USER))){
            cartService.save(cartMapper.toEntity(user));
        }
        return userMapper.toResponsePublic(user);
    }
}
