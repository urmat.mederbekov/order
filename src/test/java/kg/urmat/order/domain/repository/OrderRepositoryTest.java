package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.Order;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.data.util.enums.StatusCode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class OrderRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserRepository userRepository;

    static Order order;
    static User user;

    @BeforeAll
    static void setUp() {
        user = new User();
        user.setCreatedAt(OffsetDateTime.now());
        user.setLogin("user1");
        user.setName("john");
        user.setSurname("doe");
        user.setPassword("password");
        user.setBirthDate(LocalDate.now().minusYears(18));

        order = new Order();
        order.setStatus(StatusCode.PROCESSING);
        order.setTotalCost(BigDecimal.valueOf(2350));
        order.setCreatedAt(OffsetDateTime.now());
    }

    @Test
    void shouldFindAllByCreatedById() {
        user = userRepository.save(user);
        order.setUser(user);
        orderRepository.save(order);
        assertEquals(1, orderRepository.findAllByUserId(user.getId()).size());
    }

    @Test
    void shouldNotFindAllByCreatedById() {
        assertEquals(0, orderRepository.findAllByUserId(user.getId()).size());
    }
}