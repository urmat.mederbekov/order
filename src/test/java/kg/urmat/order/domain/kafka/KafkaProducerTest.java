package kg.urmat.order.domain.kafka;

import kg.urmat.order.domain.service.CartService;
import kg.urmat.order.domain.service.kafka.KafkaProducer;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@Testcontainers
@ActiveProfiles(profiles = "kafka")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class KafkaProducerTest {

    @Container
    static KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.3.1"));

    @Autowired
    private KafkaProducer kafkaProducer;
    @MockBean
    private CartService cartService;

    @DynamicPropertySource
    static void kafkaProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.kafka.bootstrap-servers", kafkaContainer::getBootstrapServers);
    }

    @Test
    void shouldSend() {
        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        Long userId = 2L;

        kafkaProducer.send(userId);

        verify(cartService, timeout(5000)).getByUserId(captor.capture());
    }
}