package kg.urmat.order.core.config.property;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConditionalOnProperty(value = "kafka.enabled", havingValue = "true", matchIfMissing = true)
@ConfigurationProperties(prefix = "kafka")
public record KafkaProperties(boolean enabled, TopicProperties topic) {
    public record TopicProperties(String name, int numPartitions, short replicationFactor, String groupId) {}
}
