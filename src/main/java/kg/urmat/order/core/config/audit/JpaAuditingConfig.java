package kg.urmat.order.core.config.audit;

import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.service.UserService;
import kg.urmat.order.domain.service.auth.PrincipalContextProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.time.OffsetDateTime;
import java.util.Optional;

@Configuration
@EnableJpaAuditing(
        dateTimeProviderRef = "dateTimeProvider",
        auditorAwareRef = "auditorProvider"
)
@RequiredArgsConstructor
public class JpaAuditingConfig {

    private final PrincipalContextProvider principalContextProvider;
    private final UserService userService;


    @Bean
    public DateTimeProvider dateTimeProvider() {
        return () -> Optional.of(OffsetDateTime.now());
    }

    @Bean
    public AuditorAware<User> auditorProvider(){
        return () -> principalContextProvider.isAuthenticated() ?
                Optional.of(userService.get(principalContextProvider.getCurrentPrincipal().getUserId())) :
                Optional.empty();
    }
}
