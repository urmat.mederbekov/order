package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.product.ProductCreateRequest;
import kg.urmat.order.domain.data.dto.product.ProductResponse;
import kg.urmat.order.domain.data.dto.product.ProductUpdateRequest;
import kg.urmat.order.domain.data.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductResponse toResponsePublic(Product product);

    Product toEntity(ProductCreateRequest productRequest);

    Product toEntity(ProductUpdateRequest productRequest, @MappingTarget Product product);
}
