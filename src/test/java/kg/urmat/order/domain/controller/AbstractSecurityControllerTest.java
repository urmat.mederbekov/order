package kg.urmat.order.domain.controller;

import kg.urmat.order.core.config.security.SecurityConfig;
import kg.urmat.order.domain.service.auth.CustomUserDetailService;
import kg.urmat.order.domain.service.auth.PrincipalContextProvider;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

@Import(SecurityConfig.class)
abstract class AbstractSecurityControllerTest {

    @MockBean
    protected CustomUserDetailService userDetailService;

    @MockBean(name = "principalContextProviderImpl")
    protected PrincipalContextProvider principalContextProvider;
}
