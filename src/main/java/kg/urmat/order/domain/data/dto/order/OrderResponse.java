package kg.urmat.order.domain.data.dto.order;

import kg.urmat.order.domain.data.dto.orderItem.OrderItemResponse;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.util.enums.StatusCode;
import lombok.Builder;

import java.math.BigDecimal;
import java.util.List;

@Builder
public record OrderResponse(
        Long id,
        StatusCode status,
        BigDecimal totalCost,
        List<OrderItemResponse> orderItems,
        UserResponse user
){}