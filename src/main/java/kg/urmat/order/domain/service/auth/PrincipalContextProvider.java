package kg.urmat.order.domain.service.auth;

import kg.urmat.order.domain.data.model.auth.CustomPrincipal;

public interface PrincipalContextProvider {

    CustomPrincipal getCurrentPrincipal();

    boolean isAuthenticated();

    boolean isAllowed(Long userId);
}
