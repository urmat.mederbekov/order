package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.product.ProductCreateRequest;
import kg.urmat.order.domain.data.dto.product.ProductUpdateRequest;
import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductMapperImplTest {

    private final ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);

    static Product product;

    @BeforeAll
    static void setUp(){
        product = new Product();
        product.setCode(ProductCode.IPAD);
        product.setName("mac");
        product.setPrice(1000);
        product.setDescription("very expensive");
    }

    @Test
    void toResponsePublic() {
        var response = productMapper.toResponsePublic(product);

        assertEquals(product.getPrice(), response.price());
        assertEquals(product.getDescription(), response.description());
        assertEquals(product.getName(), response.name());
        assertEquals(product.getCode(), response.code());
    }

    @Test
    void toEntityFromCreate() {
        var request = ProductCreateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(100.0)
                .description("cheap")
                .name("hp")
                .build();
        Product product = productMapper.toEntity(request);

        assertEquals(request.price(), product.getPrice());
        assertEquals(request.code(), product.getCode());
        assertEquals(request.name(), product.getName());
        assertEquals(request.description(), product.getDescription());
    }

    @Test
    void ToEntityFromUpdate() {
        var request = ProductUpdateRequest.builder()
                .code(ProductCode.COMPUTER)
                .price(200.0)
                .description("cheap")
                .name("hp")
                .build();

        Product updatedProduct = productMapper.toEntity(request, product);

        assertEquals(request.price(), updatedProduct.getPrice());
        assertEquals(request.code(), updatedProduct.getCode());
        assertEquals(request.name(), updatedProduct.getName());
        assertEquals(request.description(), updatedProduct.getDescription());
    }
}