package kg.urmat.order.domain.service;

import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.util.enums.RoleCode;

import java.util.Collection;
import java.util.Set;

public interface RoleService{

    Role get(Long id);

    Set<Role> getAllByCodes(Collection<RoleCode> roles);

    Role getByCode(RoleCode code);
}
