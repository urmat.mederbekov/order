package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.cartIem.CartItemResponse;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.data.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartMapperImplTest {

    @Mock
    private CartItemMapper cartItemMapper;
    @Mock
    private UserMapper userMapper;
    @InjectMocks
    private CartMapper cartMapper = Mappers.getMapper(CartMapper.class);

    @Test
    void toResponsePublic() {
        Cart cart = new Cart();
        cart.setUser(new User());
        cart.setItems(List.of(new CartItem()));

        when(cartItemMapper.toResponsePublic(cart.getItems().get(0))).thenReturn(CartItemResponse.builder().build());
        when(userMapper.toResponsePublic(cart.getUser())).thenReturn(UserResponse.builder().build());
        var response = cartMapper.toResponsePublic(cart);

        assertEquals(UserResponse.builder().build(), response.user());
        assertEquals(List.of(CartItemResponse.builder().build()), response.items());
    }

    @Test
    void toEntity() {
        var cart = cartMapper.toEntity(new User());

        assertNotNull(cart);
        assertEquals(new User(), cart.getUser());
    }
}