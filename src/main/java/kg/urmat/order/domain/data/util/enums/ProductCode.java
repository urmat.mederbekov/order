package kg.urmat.order.domain.data.util.enums;

public enum ProductCode {
    PHONE,
    COMPUTER,
    LAPTOP,
    EARPHONE,
    IPAD
}
