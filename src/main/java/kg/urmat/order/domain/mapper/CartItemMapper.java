package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.cartIem.CartItemCreateRequest;
import kg.urmat.order.domain.data.dto.cartIem.CartItemResponse;
import kg.urmat.order.domain.data.dto.cartIem.CartItemUpdateRequest;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.data.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = ProductMapper.class)
public interface CartItemMapper {

    @Mapping(target = "cartId", source = "cart.id")
    CartItemResponse toResponsePublic(CartItem entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "quantity", source = "request.quantity")
    CartItem toEntity(CartItemCreateRequest request, Product product, Cart cart);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "quantity", source = "request.quantity")
    CartItem toEntity(CartItemUpdateRequest request, @MappingTarget CartItem entity, Product product, Cart cart);
}
