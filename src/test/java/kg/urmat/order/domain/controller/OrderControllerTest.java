package kg.urmat.order.domain.controller;

import kg.urmat.order.core.annotation.WithMockPrincipal;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.facade.OrderFacade;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static kg.urmat.order.domain.data.util.constant.RoleConstant.ADMIN;
import static kg.urmat.order.domain.data.util.constant.Route.ORDERS_API;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrderController.class)
class OrderControllerTest extends AbstractSecurityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderFacade orderFacade;

    @Test
    @Disabled
    void search() {
    }

    @Test
    @WithMockPrincipal
    void shouldCreate() throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();

        mockMvc.perform(post(ORDERS_API))
                .andExpect(status().is(201));
        verify(orderFacade, times(1)).add(principal.getUserId());
    }

    @Test
    @WithMockUser(authorities = ADMIN)
    void shouldNotCreate() throws Exception {
        mockMvc.perform(post(ORDERS_API))
                .andExpect(status().is(401));
        verify(orderFacade, times(0)).add(any());
    }
}