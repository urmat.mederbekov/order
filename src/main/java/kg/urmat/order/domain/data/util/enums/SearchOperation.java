package kg.urmat.order.domain.data.util.enums;

public enum SearchOperation {

    EQUALITY, NEGATION, GREATER_THAN, LESS_THAN, LIKE, SORT_ASC, SORT_DESC;

    public static SearchOperation getSimpleOperation(final String input) {
        return switch (input) {
            case ":" -> EQUALITY;
            case "!" -> NEGATION;
            case ">" -> GREATER_THAN;
            case "<" -> LESS_THAN;
            case "~" -> LIKE;
            case "+", " " -> SORT_ASC;
            case "-" -> SORT_DESC;
            default -> throw new UnsupportedOperationException("Operation " + input + " is not supported");
        };
    }
}
