package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.order.OrderResponse;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {OrderItemMapper.class, UserMapper.class})
public interface OrderMapper {

    OrderResponse toResponsePublic(Order order);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    @Mapping(target = "orderItems", source = "items")
    Order toEntity(Cart cart);

}
