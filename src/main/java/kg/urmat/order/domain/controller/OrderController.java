package kg.urmat.order.domain.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import kg.urmat.order.domain.data.dto.order.OrderResponse;
import kg.urmat.order.domain.data.dto.page.PageResponse;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.facade.OrderFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import static kg.urmat.order.domain.data.util.constant.RoleConstant.ADMIN;
import static kg.urmat.order.domain.data.util.constant.RoleConstant.USER;
import static kg.urmat.order.domain.data.util.constant.Route.ORDERS_API;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@SecurityRequirement(name = "bearerAuth")
@RequestMapping(ORDERS_API)
@PreAuthorize("hasRole('" + USER + "')")
@RequiredArgsConstructor
public class OrderController {

    private final OrderFacade orderFacade;

    @GetMapping
    @PreAuthorize("hasRole('" + ADMIN + "')")
    public ResponseEntity<PageResponse<OrderResponse>> search(
            @RequestParam(value = "page", defaultValue = "0") int pageIndex,
            @RequestParam(value = "size", defaultValue = "20") int size,
            @RequestParam(name = "order", required = false) String order,
            @RequestParam(name = "and-search", required = false) String andSearch,
            @RequestParam(name = "or-search", required = false) String orSearch
    ){
        return ResponseEntity.status(OK)
                .body(orderFacade.search(pageIndex, size, order, andSearch, orSearch));
    }

    @PostMapping
    public ResponseEntity<?> create(Authentication authentication){
        CustomPrincipal customPrincipal = (CustomPrincipal) authentication.getPrincipal();
        orderFacade.add(customPrincipal.getUserId());
        return ResponseEntity.status(CREATED).build();
    }
}
