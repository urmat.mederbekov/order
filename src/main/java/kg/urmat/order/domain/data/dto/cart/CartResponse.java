package kg.urmat.order.domain.data.dto.cart;

import kg.urmat.order.domain.data.dto.cartIem.CartItemResponse;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import lombok.Builder;

import java.util.List;

@Builder
public record CartResponse(
        Long id,
        List<CartItemResponse> items,
        UserResponse user
){}