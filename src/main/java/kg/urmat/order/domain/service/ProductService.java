package kg.urmat.order.domain.service;

import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface ProductService {

    Page<Product> search(Specification<Product> build, Pageable pageable);

    Product get(Long id);

    List<Product> getAllById(List<Long> ids);

    List<Product> getAllByCode(ProductCode code);

    Product save(Product entity);
}
