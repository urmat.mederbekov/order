package kg.urmat.order.domain.service.impl;

import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.repository.CartItemRepository;
import kg.urmat.order.domain.service.CartItemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CartItemServiceImpl implements CartItemService {

    private final CartItemRepository repository;

    @Override
    public CartItem get(Long id) {
        log.info("Getting CartItem with id {}", id);
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException("CartItem", "id", id));
    }

    @Override
    public CartItem save(CartItem entity) {
        log.info("Saving CartItem {}", entity);
        return repository.save(entity);
    }

    @Override
    public void emptyCart(List<CartItem> entities) {
        log.info("Emptying CartItems {}", entities);
        repository.deleteAllInBatch(entities);
    }

    public boolean existsInCart(Long cartId, Long productId){
        log.info("Checking if CartItem with id {} exists in Cart with id {}", productId, cartId);
        return repository.existsByCartIdAndProductId(cartId, productId);
    }
}
