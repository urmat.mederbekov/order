package kg.urmat.order.domain.data.dto.user;

import kg.urmat.order.domain.data.dto.role.RoleResponse;
import lombok.Builder;

import java.time.LocalDate;
import java.util.Set;

@Builder
public record UserResponse(
        Long id,
        String login,
        String name,
        String surname,
        LocalDate birthDate,
        Set<RoleResponse> roles
){}