package kg.urmat.order.domain.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kg.urmat.order.domain.data.dto.cart.CartResponse;
import kg.urmat.order.domain.data.dto.order.OrderResponse;
import kg.urmat.order.domain.data.dto.orderItem.OrderItemResponse;
import kg.urmat.order.domain.data.dto.role.RoleResponse;
import kg.urmat.order.domain.data.dto.user.UserCreateRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.dto.user.UserUpdateRequest;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import kg.urmat.order.domain.data.util.enums.StatusCode;
import kg.urmat.order.domain.facade.CartFacade;
import kg.urmat.order.domain.facade.OrderFacade;
import kg.urmat.order.domain.facade.UserFacade;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static kg.urmat.order.domain.data.util.constant.RoleConstant.ADMIN;
import static kg.urmat.order.domain.data.util.constant.RoleConstant.USER;
import static kg.urmat.order.domain.data.util.constant.Route.USERS_API;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest extends AbstractSecurityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserFacade userFacade;

    @MockBean
    private OrderFacade orderFacade;

    @MockBean
    private CartFacade cartFacade;

    private final Long userId = 1L;

    @Test
    @Disabled
    void search() {
    }

    @Test
    @WithMockUser(authorities = ADMIN)
    void shouldGet() throws Exception {
        var response = UserResponse.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .roles(Set.of(RoleResponse.builder().build()))
                .build();

        when(principalContextProvider.isAllowed(userId)).thenReturn(true);
        when(userFacade.get(userId)).thenReturn(response);

        mockMvc.perform(get(USERS_API + "/1")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.birthDate").value(response.birthDate().toString()))
                .andExpect(jsonPath("$.name").value(response.name()))
                .andExpect(jsonPath("$.surname").value(response.surname()))
                .andExpect(jsonPath("$.roles").exists());
    }

    @Test
    @WithMockUser(authorities = ADMIN)
    void shouldCreate() throws Exception {
        var request = UserCreateRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("vito1")
                .password("231231")
                .build();

        var response= UserResponse.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .roles(Set.of(RoleResponse.builder().build()))
                .build();

        when(userFacade.add(request)).thenReturn(response);

        mockMvc.perform(post(USERS_API)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.birthDate").value(response.birthDate().toString()))
                .andExpect(jsonPath("$.name").value(response.name()))
                .andExpect(jsonPath("$.surname").value(response.surname()))
                .andExpect(jsonPath("$.roles").exists());

        verify(userFacade, times(1)).add(request);
    }

    @Test
    @WithMockUser(authorities = USER)
    void shouldNotCreate() throws Exception {
        var request = UserCreateRequest.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .name("vito")
                .surname("scaletta")
                .login("vito1")
                .password("231231")
                .build();

        mockMvc.perform(post(USERS_API)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(401));

        verify(userFacade, times(0)).add(request);
    }

    @Test
    @WithMockUser(authorities = ADMIN)
    void shouldUpdate() throws Exception {
        var request = UserUpdateRequest.builder()
                .name("avatar")
                .surname("ang")
                .birthDate(LocalDate.now().minusYears(18))
                .roleCodes(Set.of(RoleCode.USER))
                .build();

        var response = UserResponse.builder()
                .name("avatar")
                .surname("ang")
                .birthDate(LocalDate.now().minusYears(18))
                .roles(Set.of(RoleResponse.builder().build()))
                .build();

        when(userFacade.update(request, userId)).thenReturn(response);

        mockMvc.perform(put(USERS_API + "/" + userId)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.birthDate").value(response.birthDate().toString()))
                .andExpect(jsonPath("$.name").value(response.name()))
                .andExpect(jsonPath("$.surname").value(response.surname()))
                .andExpect(jsonPath("$.roles").exists());

        verify(userFacade, times(1)).update(request, userId);
    }

    @Test
    @WithMockUser(authorities = USER)
    void shouldNotUpdate() throws Exception {
        var request = UserUpdateRequest.builder()
                .name("avatar")
                .surname("ang")
                .birthDate(LocalDate.now().minusYears(18))
                .roleCodes(Set.of(RoleCode.USER))
                .build();

        mockMvc.perform(put(USERS_API + "/" + userId)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(401));

        verify(userFacade, times(0)).update(request, userId);
    }

    @Test
    @WithMockUser(authorities = USER)
    void shouldGetCart() throws Exception {
        var response = CartResponse.builder()
                .id(1L)
                .user(UserResponse.builder().build())
                .items(new ArrayList<>())
                .build();
        when(principalContextProvider.isAllowed(userId)).thenReturn(true);
        when(cartFacade.getByUserId(userId)).thenReturn(response);
        mockMvc.perform(get(USERS_API + "/" + userId +"/cart")
                .contentType(APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.id").value(response.id()))
                .andExpect(jsonPath("$.user").exists())
                .andExpect(jsonPath("$.items").value(response.items()));
    }

    @Test
    @WithMockUser(authorities = USER)
    void shouldNotGetCart() throws Exception {
        when(principalContextProvider.isAllowed(userId)).thenReturn(true);
        when(cartFacade.getByUserId(userId)).thenReturn(null);
        mockMvc.perform(get(USERS_API + "/" + userId +"/cart")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(authorities = USER)
    void shouldGetOrders() throws Exception {
        List<OrderResponse> response = List.of(OrderResponse.builder()
                .id(1L)
                .user(UserResponse.builder().build())
                .totalCost(BigDecimal.valueOf(1235))
                .orderItems(List.of(OrderItemResponse.builder().build()))
                .status(StatusCode.SHIPPING)
                .build());
        when(principalContextProvider.isAllowed(userId)).thenReturn(true);
        when(orderFacade.getAllByUserId(userId)).thenReturn(response);
        mockMvc.perform(get(USERS_API + "/" + userId +"/orders")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$").exists());
    }

    @Test
    @WithMockUser(authorities = USER)
    void shouldNotGetOrders() throws Exception {
        when(principalContextProvider.isAllowed(userId)).thenReturn(true);
        when(orderFacade.getAllByUserId(userId)).thenReturn(null);
        mockMvc.perform(get(USERS_API + "/" + userId +"/orders")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$").doesNotExist());
    }
}