package kg.urmat.order.domain.data.util.enums;

public enum RoleCode {
    ADMIN,
    USER
}
