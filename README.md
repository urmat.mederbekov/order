# Order

### Requirements

* Java version 17
* Spring Boot version 3.1.1
* Postgres 10
* Kafka 3.3.2

### Features

* JWT Based Stateless Authentication
* Spring JPA Data
* Apache Kafka
* N-layer Architecture (Controller, Facade, Mapper, Service, Repository)
* OpenAPI with JWT
* Custom Validating Annotations
* Global Exception Handling
* Criteria Api Search, Pagination and Ordering
* Unit & Integration Testing
* Mapstruct
* Redis
* Gitlab CI

### Deploy

```
git clone https://gitlab.com/urmat.mederbekov/order.git
```
```
gradle clean build
```

### Notes
To run tests you need a running Docker.

If you want to use Swagger you need jwt token. You can get it through login. Once you have it use green "Authorize" button. As a value enter "Bearer your-JWT-token"

There's one user in the system. login - admin, password - 123

https://prnt.sc/1a8qo6z

How search works: https://prnt.sc/1al0ciw

Only /api/auth/** url is unprotected

Admin can create a user with any role(s)

User can register himself with role USER

Only user with role USER can order:

User

1) fills his cart with cart items
2) orders (his cart items are converted to order items, the cart is emptied and order is created)

## Author

* https://gitlab.com/urmat.mederbekov
