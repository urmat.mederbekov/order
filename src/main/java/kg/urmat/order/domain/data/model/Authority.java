package kg.urmat.order.domain.data.model;

import jakarta.persistence.*;
import kg.urmat.order.domain.data.model.base.BaseEntity;
import kg.urmat.order.domain.data.util.enums.AuthorityCode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "authorities")
public class Authority extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 980986764090520201L;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, unique = true)
    private AuthorityCode code;

    @Column(nullable = false)
    private String name;
}
