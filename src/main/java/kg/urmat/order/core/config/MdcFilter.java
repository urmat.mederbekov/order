package kg.urmat.order.core.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.UUID;

import static java.util.Objects.isNull;

@Order(1)
@Component
public class MdcFilter extends OncePerRequestFilter {

    private static final String REQUEST_ID = "X-Request-ID";

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws ServletException, IOException {
        String requestId = request.getHeader(REQUEST_ID);

        if (isNull(requestId) || requestId.isBlank())
            requestId = UUID.randomUUID().toString();

        MDC.put(REQUEST_ID, requestId);
        chain.doFilter(request, response);
        MDC.remove(REQUEST_ID);
    }
}