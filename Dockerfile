# -------------------------------------- GENERATING JAR --------------------------------------------
# Use a Maven image as the build stage for generating the executable JAR file
FROM maven:3.9.2-eclipse-temurin-17-alpine as builder
WORKDIR app
# Copy the project's POM file to the build stage and download dependencies
COPY pom.xml .
RUN mvn dependency:go-offline
# Copy the project's source code to the build stage and build the executable JAR file
COPY src src
RUN mvn install -Dmaven.test.skip=true && rm -rf /root/.m2

# -------------------------------------- EXTRACTING LAYERS --------------------------------------------
# Use an OpenJDK image as the intermediate build stage for extracting layers from the JAR file
FROM openjdk:19-alpine as intermediate_builder
WORKDIR app
# Copy the generated JAR file from the Maven build stage to the intermediate build stage
ARG JAR_FILE=app/target/*.jar
COPY --from=builder $JAR_FILE application.jar
# Extract the layers from the JAR file and delete the JAR file
RUN java -Djarmode=layertools -jar application.jar extract && rm application.jar

# -------------------------------------- RUNNING APP --------------------------------------------
# Use another OpenJDK image as the final build stage for running the application
FROM openjdk:19-alpine
WORKDIR app
# Create a new group and user named "appgroup" and "appuser" inside the container
RUN addgroup -S appgroup && adduser -S appuser -G appgroup
# Copy the extracted layers from the intermediate build stage to the final build stage
COPY --from=intermediate_builder --chown=appuser:appgroup /app/dependencies/ .
COPY --from=intermediate_builder --chown=appuser:appgroup /app/snapshot-dependencies/ .
COPY --from=intermediate_builder --chown=appuser:appgroup /app/spring-boot-loader/ .
COPY --from=intermediate_builder --chown=appuser:appgroup /app/application/ .
# Expose the container's port 8080 for incoming connections
EXPOSE 8080
# Set the default user for the container to "appuser" for improved security
USER appuser
# Set labels on the Docker image for improved visibility and traceability
LABEL maintainer="Urmat Mederbekov <urmat.mederbekov@gmail.com>"
# Set the entrypoint to run the application using the Spring Boot loader
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]