package kg.urmat.order.domain.data.dto.product;

import kg.urmat.order.domain.data.util.enums.ProductCode;
import lombok.Builder;

@Builder
public record ProductResponse(
        Long id,
        ProductCode code,
        String name,
        String description,
        Double price,
        Long quantity
){}