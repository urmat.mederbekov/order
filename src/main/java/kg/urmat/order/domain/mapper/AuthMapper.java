package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.auth.CustomerPrincipalResponse;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.data.model.auth.CustomPrincipal;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface AuthMapper {

    CustomerPrincipalResponse toResponsePrivate(CustomPrincipal user, String token);

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "username", source = "login")
    @Mapping(target = "roleCodes", source = "roles")
    @Mapping(target = "authorities", source = "roles")
    @Mapping(target = "cartId", source = "cart.id")
    CustomPrincipal toEntity(User user);

    default Set<RoleCode> mapRoleCodes(Set<Role> roles){
        return roles.stream().map(Role::getCode).collect(Collectors.toSet());
    }

    default Set<GrantedAuthority> mapAuthorities(Set<Role> roles){

        Set<GrantedAuthority> rolesAndAuthorities = new HashSet<>();

        rolesAndAuthorities.addAll(roles.stream()
                .flatMap(role -> role.getAuthorities().stream()
                        .map(authority -> new SimpleGrantedAuthority(authority.getCode().name()))
                ).collect(Collectors.toSet()));

        rolesAndAuthorities.addAll(roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getCode().name()))
                .collect(Collectors.toSet()));

        return rolesAndAuthorities;
    }
}
