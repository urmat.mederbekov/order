package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.page.PageResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PageMapper {

    @Mapping(target = "content", source = "dtos")
    @Mapping(target = "page", source = "page.pageable.pageNumber")
    PageResponse toResponse(Page page, List dtos);
}
