package kg.urmat.order.core.annotation;

import kg.urmat.order.core.annotation.validator.WithMockPrincipalSecurityContextFactory;
import kg.urmat.order.domain.data.util.enums.AuthorityCode;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockPrincipalSecurityContextFactory.class)
public @interface WithMockPrincipal {

    String username() default "user";
    RoleCode role() default RoleCode.USER;
    AuthorityCode authority() default AuthorityCode.READ;

}
