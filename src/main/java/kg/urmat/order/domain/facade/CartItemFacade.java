package kg.urmat.order.domain.facade;

import jakarta.validation.Valid;
import kg.urmat.order.domain.data.dto.cartIem.CartItemCreateRequest;
import kg.urmat.order.domain.data.dto.cartIem.CartItemResponse;
import kg.urmat.order.domain.data.dto.cartIem.CartItemUpdateRequest;
import org.springframework.validation.annotation.Validated;

@Validated
public interface CartItemFacade {

    CartItemResponse add(@Valid CartItemCreateRequest cartItemRequest, Long userId, Long cartId);

    CartItemResponse update(@Valid CartItemUpdateRequest cartItemRequest, Long entityId, Long userId);
}
