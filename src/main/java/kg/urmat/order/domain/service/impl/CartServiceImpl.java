package kg.urmat.order.domain.service.impl;

import kg.urmat.order.core.error.exception.NoSuchElementFoundException;
import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.repository.CartRepository;
import kg.urmat.order.domain.service.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private final CartRepository repository;

    @Override
    public Cart get(Long id) {
        log.info("Getting Cart with id {}", id);
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException("Cart", "id", id));
    }

    @Override
    public Cart getByUserId(Long userId) {
        log.info("Getting Cart by user id {}", userId);
        return repository.findByUserId(userId)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No Cart found, whose User id is %d", userId)));
    }

    @Override
    public Cart save(Cart entity) {
        log.info("Saving Cart {}", entity);
        return repository.save(entity);
    }
}
