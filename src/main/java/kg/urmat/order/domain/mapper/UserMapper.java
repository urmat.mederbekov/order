package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.dto.user.UserCreateRequest;
import kg.urmat.order.domain.data.dto.user.UserRegisterRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.dto.user.UserUpdateRequest;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.model.User;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.Set;

@Mapper(componentModel = "spring", uses = RoleMapper.class, collectionMappingStrategy = CollectionMappingStrategy.TARGET_IMMUTABLE)
public interface UserMapper {

    UserResponse toResponsePublic(User user);

    @Mapping(target = "password", source = "password")
    User toEntity(UserCreateRequest userRequest, String password, Set<Role> roles);

    @Mapping(target = "password", source = "password")
    User toEntity(UserRegisterRequest userRequest, String password, Set<Role> roles);

    User toEntity(UserUpdateRequest userRequest, @MappingTarget User user, Set<Role> roles);
}
