package kg.urmat.order.domain.mapper;

import kg.urmat.order.domain.data.model.Authority;
import kg.urmat.order.domain.data.model.Role;
import kg.urmat.order.domain.data.util.enums.AuthorityCode;
import kg.urmat.order.domain.data.util.enums.RoleCode;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RoleMapperImplTest {

    private final RoleMapper roleMapper = Mappers.getMapper(RoleMapper.class);

    @Test
    void toResponsePubic() {
        Authority authority = new Authority();
        authority.setCode(AuthorityCode.CREATE);

        Role role = new Role();
        role.setName("admin");
        role.setCode(RoleCode.ADMIN);
        role.setAuthorities(Set.of(authority));

        var response = roleMapper.toResponsePubic(role);

        assertEquals(role.getCode(), response.code());
        assertEquals(role.getName(), response.name());
        assertEquals(role.getAuthorities().stream().map(Authority::getCode).collect(Collectors.toSet()), response.authorities());
    }
}