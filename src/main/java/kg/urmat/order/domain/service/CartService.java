package kg.urmat.order.domain.service;

import kg.urmat.order.domain.data.model.Cart;

public interface CartService {

    Cart get(Long id);

    Cart getByUserId(Long userId);

    Cart save(Cart entity);

}
