package kg.urmat.order.domain.data.model;

import jakarta.persistence.*;
import kg.urmat.order.domain.data.model.base.AuditedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true, exclude = "cart")
@Entity
@Table(name = "users")
@NamedEntityGraph(name = "user.roles", attributeNodes = @NamedAttributeNode("roles"))
public class User extends AuditedEntity {

    @Serial
    private static final long serialVersionUID = 138507141940632984L;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false)
    private LocalDate birthDate;

    private boolean enabled = true;

    @ManyToMany
    @JoinTable(name = "m2m_user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @OneToOne(mappedBy = "user")
    private Cart cart;

}
