package kg.urmat.order.domain.facade.impl;

import kg.urmat.order.domain.data.dto.order.OrderResponse;
import kg.urmat.order.domain.data.dto.page.PageResponse;
import kg.urmat.order.domain.data.dto.page.SearchCriteria;
import kg.urmat.order.domain.facade.OrderFacade;
import kg.urmat.order.domain.mapper.OrderMapper;
import kg.urmat.order.domain.mapper.PageMapper;
import kg.urmat.order.domain.data.model.Order;
import kg.urmat.order.domain.service.OrderService;
import kg.urmat.order.domain.service.builder.SpecificationsBuilder;
import kg.urmat.order.domain.service.kafka.KafkaProducer;
import kg.urmat.order.domain.util.SearchCriteriaParserUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderFacadeImpl implements OrderFacade {

    private final OrderService orderService;
    private final KafkaProducer kafkaProducer;
    private final OrderMapper orderMapper;
    private final PageMapper pageMapper;

    @Override
    public PageResponse<OrderResponse> search(int pageIndex, int size, String order, String andSearch, String orSearch) {
        List<SearchCriteria> andParams = SearchCriteriaParserUtils.parse(andSearch);
        List<SearchCriteria> orParams = SearchCriteriaParserUtils.parse(orSearch);
        List<SearchCriteria> orderParams = SearchCriteriaParserUtils.parse(order);

        SpecificationsBuilder<Order> builder = new SpecificationsBuilder<>(andParams, orParams);

        List<Sort> sorts = builder.generateSortList(orderParams);
        Sort sort = builder.andSort(sorts);
        Pageable pageable = PageRequest.of(pageIndex, size, sort);

        Page<Order> page = orderService.search(builder.build(), pageable);
        List<OrderResponse> dtos = page.getContent().stream()
                .map(orderMapper::toResponsePublic)
                .collect(Collectors.toList());

        return pageMapper.toResponse(page, dtos);
    }

    @Override
    public void add(Long userId) {
        kafkaProducer.send(userId);
    }

    @Override
    public List<OrderResponse> getAllByUserId(Long userId) {
        return orderService.getAllByUserId(userId).stream().map(orderMapper::toResponsePublic).collect(Collectors.toList());
    }
}
