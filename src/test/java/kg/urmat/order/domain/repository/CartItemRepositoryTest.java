package kg.urmat.order.domain.repository;

import kg.urmat.order.domain.data.model.Cart;
import kg.urmat.order.domain.data.model.CartItem;
import kg.urmat.order.domain.data.model.Product;
import kg.urmat.order.domain.data.model.User;
import kg.urmat.order.domain.data.util.enums.ProductCode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CartItemRepositoryTest {

    @Autowired
    private CartItemRepository repository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private UserRepository userRepository;

    static CartItem cartItem;
    static Product product;
    static Cart cart;
    static User user;

    @BeforeAll
    static void setUp() {
        product = new Product();
        product.setCode(ProductCode.COMPUTER);
        product.setName("mac");
        product.setPrice(100);
        product.setDescription("very expensive");

        cartItem = new CartItem();
        cartItem.setQuantity(2);

        cart = new Cart();
        cart.setCreatedAt(OffsetDateTime.now());

        user = new User();
        user.setCreatedAt(OffsetDateTime.now());
        user.setLogin("user1");
        user.setName("john");
        user.setSurname("doe");
        user.setPassword("password");
        user.setBirthDate(LocalDate.now().minusYears(18));
    }

    @Test
    void shouldExistsByCartIdAndProductId() {
        user = userRepository.save(user);
        cart.setUser(user);
        cart = cartRepository.save(cart);
        product = productRepository.save(product);
        cartItem.setCart(cart);
        cartItem.setProduct(product);
        repository.save(cartItem);
        assertTrue(repository.existsByCartIdAndProductId(cart.getId(), product.getId()));
    }

    @Test
    void shouldNotExistsByCartIdAndProductId() {
        assertFalse(repository.existsByCartIdAndProductId(cart.getId(), product.getId()));
    }
}