package kg.urmat.order.domain.data.util.enums;

public enum StatusCode {
    PROCESSING,
    SHIPPING,
    COMPLETED
}
