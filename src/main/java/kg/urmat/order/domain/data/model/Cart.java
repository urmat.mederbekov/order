package kg.urmat.order.domain.data.model;

import jakarta.persistence.*;
import kg.urmat.order.domain.data.model.base.AuditedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "carts")
@NamedEntityGraph(
        name = "cart.items",
        attributeNodes =
                {
                        @NamedAttributeNode("items"),
                        @NamedAttributeNode(value = "user", subgraph = "subgraph.user")
                },
        subgraphs = {
                @NamedSubgraph(
                        name = "subgraph.user", attributeNodes = {@NamedAttributeNode("roles")}
                )
        })
public class Cart extends AuditedEntity {

    @Serial
    private static final long serialVersionUID = -2306042197151239081L;

    @OneToMany(mappedBy = "cart")
    private List<CartItem> items;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;
}
