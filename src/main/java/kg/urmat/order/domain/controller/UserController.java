package kg.urmat.order.domain.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import kg.urmat.order.domain.data.dto.cart.CartResponse;
import kg.urmat.order.domain.data.dto.order.OrderResponse;
import kg.urmat.order.domain.data.dto.page.PageResponse;
import kg.urmat.order.domain.data.dto.user.UserCreateRequest;
import kg.urmat.order.domain.data.dto.user.UserResponse;
import kg.urmat.order.domain.data.dto.user.UserUpdateRequest;
import kg.urmat.order.domain.facade.CartFacade;
import kg.urmat.order.domain.facade.OrderFacade;
import kg.urmat.order.domain.facade.UserFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static kg.urmat.order.domain.data.util.constant.RoleConstant.ADMIN;
import static kg.urmat.order.domain.data.util.constant.Route.USERS_API;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@SecurityRequirement(name = "bearerAuth")
@RequestMapping(USERS_API)
@PreAuthorize("hasRole('" + ADMIN + "')")
@RequiredArgsConstructor
public class UserController {

    private final UserFacade userFacade;
    private final CartFacade cartFacade;
    private final OrderFacade orderFacade;

    @GetMapping
    public ResponseEntity<PageResponse<UserResponse>> search(
            @RequestParam(value = "page", defaultValue = "0") int pageIndex,
            @RequestParam(value = "size", defaultValue = "20") int size,
            @RequestParam(name = "order", required = false) String order,
            @RequestParam(name = "and-search", required = false) String andSearch,
            @RequestParam(name = "or-search", required = false) String orSearch
    ){
        return ResponseEntity.status(OK)
                .body(userFacade.search(pageIndex, size, order, andSearch, orSearch));
    }

    @PreAuthorize("@principalContextProviderImpl.isAllowed(#id)")
    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> get(@PathVariable Long id){
        return ResponseEntity.status(OK).body(userFacade.get(id));
    }

    @PostMapping
    public ResponseEntity<UserResponse> create(@RequestBody UserCreateRequest user){
        return ResponseEntity.status(CREATED).body(userFacade.add(user));
    }

    @PutMapping(("/{id}"))
    public ResponseEntity<UserResponse> update(@RequestBody UserUpdateRequest user, @PathVariable Long id){
        return ResponseEntity.status(OK).body(userFacade.update(user, id));
    }

    @PreAuthorize("@principalContextProviderImpl.isAllowed(#id)")
    @GetMapping("/{id}/cart")
    public ResponseEntity<CartResponse> getCart(@PathVariable Long id){
        return ResponseEntity.status(OK).body(cartFacade.getByUserId(id));
    }

    @PreAuthorize("@principalContextProviderImpl.isAllowed(#id)")
    @GetMapping("/{id}/orders")
    public ResponseEntity<List<OrderResponse>> getOrders(@PathVariable Long id){return ResponseEntity.status(OK).body(orderFacade.getAllByUserId(id));}

}
